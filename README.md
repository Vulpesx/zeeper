# Zeeper

A playlist tool for [Zeepkist](https://store.steampowered.com/app/1440670/Zeepkist/)

create playlists and manage mods

## Demo
![demo](./demo.gif)

### Installation

#### Cargo
`cargo install zeeper`

if you're building from source `cargo install --path /path/to/repo`

#### Binary
download binary from [releases](https://codeberg.org/Vulpesx/zeeper/releases) and run it.

### Executing Program

in the terminal run `zeeper [OPTIONS] <CMD>`

> zeeper automaticaly finds the necissary directories
> and uses the steam id of the last steam user

> use the options or env args if you wish to overide
> or if you are on a platform which zeeper cant do that magic
> (zeeper will tell you if it cant magic :3 )

for more information use the help commands
#### Options
 - `-h` `--help`
 - `-V` `--version`
#### Commands
 - `playlist (alias p) [OPTIONS] <CMD>`: playlist related commands
 - `id <TYPE> <ID>`: get id information from gtr
 - `mod (alias: m) [OPTIONS] <CMD>`: mod related commands
 - `completions <SHELL> > /path/to/save/to`: generate shell completions
 - `help [CMD]`: show help

##### Playlist Options
 - `-d` `--dir` `<DIR>`: output dir for playlists
 - `--purge`: purge duplicate tracks from playlist
 - `--purge-strata` `<STRATA>`: the purge strategy to use (recommended to not change this, the default is fine)
 - `--stdout`: print to stdout instead of saving a file
 - `-m` `--metadata` `name,round-time[in seconds],shuffle[y/n]`: the meta data for the playlist, if omitted you will get a nice prompt

##### Playlist Commands
 - `random (alias rand) [AMOUNT: default 10]`: get random [AMOUNT] random tracks
 - `wr [OPTIONS] [ID: can get from env]`: get tracks user \<ID\> has wr on
 - `pb [OPTIONS] [ID: can get from env]`: get tracks user \<ID\> has pb on
 - `hash`: create a playlist from a `,` separated list of zworpshop hashes
 - `workshop`: create a playlist from a `,` separated list of workshop id's
 - `merge <ONE> <TWO>`: merge two playlists
 - `help [CMD]`: show help

##### Mod Options
 - `-k` `--key` `<KEY>`: an api key for modio, get from your [modio account](https://mod.io/me)
 - `-d` `--dir` `<DIR>`: the dir to install mods in (recommended to use the env var) (usually points to [zeepkist steam dir])

##### Mod Commands
> zeeper is compatible with both the old modkist and modkist revamped

> a * means it requires an api key to work
 - `install-bepinex`: installs bepinex, (the mod loader for zeepkist)
    - `--linux-setup`: only perform linux/unix steps to make bepinex work
    - `--flatpak`: use protontricks via flatpak
 - `login (alias: l)`: will ask for api key and save it for you
 - `list (alias: ls)`: list installed mods
> list may require an api key to find mods if there isnt a saved manifest (there is unless this is your first ever mod command)
 - *`check (alias: c)`: will check for updates
 - *`update (alias: u)`: updates mods
 - *`install (alias: i)`: install a mod, has nice cli prompts
 - *`reinstall (alias: ri)`: reinstall mods quickly, faster than using `install` a bunch
 - *`find (alias: f)`: scans for installed mods (will ignore and replace existing manifest) (other cmds will do this automatically if there is no manifest)
 - `remove (alias: rm)`: remove a mod, **will delete all of the mod, remove any files you want to keep**
 - `sideload (alias: s) <PATH>`: install a local mod ie. a file or folder

##### Id Command
> if you have not used gtr ingame at least once this will be unavailable as you wont be in the database

the id command is useful to get your gtr user info, you can use either your gtr, steam or discord id to do this\
eg `zeeper id gtr <ID>` to get your info using a gtr id\
or `zeeper id steam <ID>` to get your info using a steam id
> most people will not have their discord linked to gtr
the output looks something like this
```txt
=== User ===
gtr id: <id>
steam id: <id>
steam name: <name>
discord id: [not linked | -1 | <id>]
```


## Help

if levels don't load, that is a steam/Zeepkist issue. Or they changed file formats :3

## Author

me

## License

This project is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0) license - see [LICENSE](LICENSE.txt) file for details.

## Acknowledgements

ThunderNerd for creating [GTR](https://zeepkist-gtr.com) and [zworpshop](https://api.zworpshop.com/swagger/index.html#/)\
jdx and roele for [demand](https://github.com/jdx/demand) (the fancy cli shit)
