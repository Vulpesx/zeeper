## Version history

- 0.1.0
  - favorites
  - random
  - hot
  - popular
- 1.0.0
  - windows/mac support (just changed to Path instead of strings to write the file)
- 1.1.0
  - now can get levels from records
  - updated hot and popular, now can take limit and offset args
- 1.1.1
  - I forgot to remove an early return lol
- 1.1.2
  - streamlined inner workings in prep for possible GUI
- 1.2.0
  - now uses Zworpshop where possible (only random tracks) (can use GTR with -G)
  - added macros to use internally
- 1.2.1
  - finally updated to new web API, so it works now
  - removed arguments that are no longer used
  - ZEEP_PLAYLIST_DIR env variable is used, so you don't have to specify output Dir
  - Favorites limit default is now 100
  - Hot and Popular no longer have arguments due to new GTR/Zworpshop API
  - updated macros for new web API's
  - little extra info outputted
- 1.2.2
  - removes duplicate tracks based on track UID
- 1.3.0
  - bunch of internal changes for upcoming update
  - Command changes!!!
    - Records doesn exist anymore
      use `wr` and `pb` instead `recent` doesnt exist anymore
    - `hash` and `workshop` accept a ',' separated list
    - `favorites` take a gtr id only, limit and offset args are dead,
    - `id-steam` gets your gtr id, using your steam id
  - also playlists are now formatted so goodbye to the ONE LOOONG LINE we had before
- 1.3.1
  - fixed json parsing error for `wr` and `pb`
- 1.3.2
  - `favorites` now works the way it used to
    it has `-l, --limit` and `-o, --offset` args again
    aswell as `-S, --steamid`, it also works now :P
  - `id-discord` added, works how youd expect
  - `workshop` now can take a list of ids, it only worked with one before
  though that was a bug
  - output is more user friendly
- 1.3.3
  - added `--no-purge` fag, this will increase the chance
  of a workshop loading error.
  - zeeper now uses `ZEEP_GTR_ID` environment var
  so you dont have to put your id everytime
  - added `merge` command to merge playlists
  and you dont have to provide a full path, it will check
  the current dir for the playlists, then it checks your playlist dir
  (which is `--dir` or `ZEEP_PLAYLIST_DIR`)
  - plus internal changes
- 1.4.0
  - changed `--no-purge` to `--purge` and is off by default,
  - added `-V --version` option
  - added `-m --metadata` to provide playlist metadata instead of through the interactive input ie `-m name,round,shuffle ...`
  - added `--stdout` prints to stdout instead of saving to a file.
  - added `japi` command to do a json api query to gtr. this makes zeeper extremely powerful as a playlist tool
  - fixed issue when supplying a file to `-d --dir ZEEP_OUT_DIR` zeeper would previously fail if the file didnt exist,
  that is no longer the case it just works now
- 1.4.1
  - updated japi help to be more helpful
  - internal refactoring
  - fix purge didnt work with `--stdout` flag
  - fix playlist level count didnt update when purged
  - added purge strategies `--purge-strata` can be `uid` or `hash`
    uid is the old method, hash is new and much better
- 1.5.0
  - manage mods with the `mod` cmd
    - compatible with modkist and modkist revamped
    - almost all commands have nice prompts
    - `install` will search and install mods
    - `update` will update installed mods
    - `check` will check for updates
    - `remove` will remove selected installed mods
    - `list` will list installed mods
    - `login` will save a modio api key for use by other commands
    - `find` will search for installed mods and add them to a manifest
            that is used by the other mods, they will run find automatically
            if no manifest is found
    - `sideload` installs a mod locally ie from a file or dir instead of from modio
  - playlist commands have been moved under the `playlist command`
    - `search` will search zworpshop for tracks, has a nice prompt
    - added spinners to most commands
    - the metadata has a nice prompt now
  - `id` provides more information and can accept steam, gtr and discord id's
  - many bug fixes, could not possibly list them, even if i remembered them
- 1.5.1
  - auto locate zeepkist install location
  - auto locate zeepkist playlists location
  - use last steam user to play zeepkist if no id is supllied to cmds that use id's
- 1.5.2
  - fix duplicate sideloads
