use anyhow::{bail, Result};
use std::{
    fs,
    path::{Path, PathBuf},
};
use ureq::Response;

use zeeper::{
    gtr::{self, GtrResponse},
    gtr_query,
    playlist::{PlaylistMeta, PurgeStrata},
    zworp_query,
    zworpshop::{self, ZworpResponse},
    Playlist, Query,
};

use crate::cli::{IdType, PlaylistCli, PlaylistCmd};

#[cfg(unix)]
fn get_playlist_dir() -> Option<PathBuf> {
    let sd = steamlocate::SteamDir::locate().ok()?;
    Some(sd.path()
                .join("steamapps/compatdata/1440670/pfx/drive_c/users/steamuser/AppData/Roaming/Zeepkist/Playlists"))
}

#[cfg(target_os = "windows")]
fn get_playlist_dir() -> Option<PathBuf> {
    dirs::data_dir().map(|d| d.join("Zeepkist/Playlists"))
}

#[cfg(not(any(unix, target_os = "windows")))]
fn get_playlist_dir() -> Option<PathBuf> {
    eprintln!("cannot find playlist dir on this platform");
    eprintln!("provide a path with cli arg or env variable");
    None
}

fn save_playlist(path: Option<PathBuf>, playlist: &Playlist, stdout: bool) {
    let playlist_out = match serde_json::to_string_pretty(playlist) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("error saving playlist {e}");
            return;
        }
    };
    if stdout {
        println!("{playlist_out}");
        return;
    }
    let Some(mut path) = path.or_else(get_playlist_dir) else {
        eprintln!("could not get the playlist dir");
        return;
    };
    if path.is_dir() {
        path.push(playlist.get_name());
    }
    path.set_extension("zeeplist");

    if let Err(e) = fs::write(&path, playlist_out) {
        eprintln!("error saving playlist {e}");
        return;
    };
    println!("Saved: {path:?}");
}

pub fn fetch_query(query: Query) -> Result<Response> {
    demand::Spinner::new("waiting for response").run(|_| Ok(ureq::get(&query.value()).call()?))?
}

fn fetch_levels(query: Query) -> Result<Response> {
    match query {
        Query::Gtr(q) => {
            let hashes = demand::Spinner::new("waiting for GTR")
                .run(|_| -> Result<String> {
                    let response = ureq::get(&q).call()?.into_reader();
                    let gr: GtrResponse = serde_json::from_reader(response)?;

                    Ok(gtr::get_levels_hash(gr.levels))
                })
                .expect("error getting hashed")?;

            fetch_levels(zworp_query!(levels / hashes, false, false, hashes))
        }
        Query::Zworpshop(q) => {
            demand::Spinner::new("waiting for Zworpshop").run(|_| Ok(ureq::get(&q).call()?))?
        }
    }
}

fn playlist_from_query(query: Query, cli: PlaylistCli) {
    let levels = match fetch_levels(query) {
        Ok(j) => j,
        Err(e) => {
            eprintln!("error fetching levels: {e}");
            return;
        }
    };
    let meta = cli.metadata.unwrap_or_else(interactive_playlist_metadata);
    let levels = serde_json::from_reader::<_, Vec<zworpshop::Level>>(levels.into_reader())
        .expect("error parsing zworpshop response")
        .into_iter()
        .map(|l| l.into())
        .collect();
    let mut playlist = Playlist::new(meta, levels);
    if cli.purge {
        println!(
            "purged {} tracks",
            playlist.purge_duplicate(cli.purge_strata)
        );
    } else if demand::Confirm::new("Purge")
        .description("remove duplicates")
        .run()
        .expect("error getting user input")
    {
        let strata = demand::Select::new("Strata")
            .description("purge strategy")
            .options(vec![
                demand::DemandOption::with_label("Hash (default)", PurgeStrata::Hash),
                demand::DemandOption::with_label("Uid (old)", PurgeStrata::Uid),
            ])
            .filterable(true)
            .run()
            .unwrap_or(PurgeStrata::Hash);
        println!("purged {} tracks", playlist.purge_duplicate(strata));
    }
    save_playlist(cli.dir, &playlist, cli.stdout);
}

fn get_latest_steam_player() -> Option<u64> {
    let sd = steamlocate::SteamDir::locate().ok()?;
    let (app, _) = sd.find_app(1440670).ok()??;
    app.last_user
}

#[allow(unreachable_code, unused_variables)]
pub fn run_cmd(cli: PlaylistCli) {
    println!("playlist commands are dissabled due to the new gtr api");
    return;
    match &cli.cmd {
        PlaylistCmd::Favorite {
            id,
            steamid,
            limit,
            offset,
        } => {
            let q = if let Some(id) = id {
                gtr_query!(favorites, id, limit, offset, *steamid)
            } else {
                let Some(id) = get_latest_steam_player() else {
                    eprintln!("could not get the latest player");
                    return;
                };
                gtr_query!(favorites, id, limit, offset, true)
            };
            playlist_from_query(q, cli)
        }
        PlaylistCmd::Random { amount } => {
            playlist_from_query(zworp_query!(levels / random, amount), cli)
        }
        PlaylistCmd::Hot => playlist_from_query(gtr_query!(levels / hot), cli),
        PlaylistCmd::Popular => playlist_from_query(gtr_query!(levels / popular), cli),
        PlaylistCmd::Wr { id, limit, offset } => {
            let q = if let Some(id) = id {
                gtr_query!(wr / usr, id, limit, offset)
            } else {
                let Some(id) = get_latest_steam_player() else {
                    eprintln!("could not get the latest player");
                    return;
                };
                // needs to know the type of None
                gtr_query!(records / wr, id, limit, offset, None::<u8>, true)
            };
            playlist_from_query(q, cli)
        }
        PlaylistCmd::Pb { id, limit, offset } => {
            let q = if let Some(id) = id {
                gtr_query!(pb / usr, id, limit, offset)
            } else {
                let Some(id) = get_latest_steam_player() else {
                    eprintln!("could not get the latest player");
                    return;
                };
                gtr_query!(records / usr, id, limit, offset, None::<u8>, true)
            };
            playlist_from_query(q, cli)
        }
        PlaylistCmd::Hash { hashes } => {
            playlist_from_query(zworp_query!(levels / hashes, false, false, hashes), cli)
        }
        PlaylistCmd::Workshop { id } => {
            playlist_from_query(zworp_query!(levels / workshop, id, false, false), cli)
        }
        PlaylistCmd::Merge { one, two } => {
            let mut playlist = match merge(one, two, &cli.dir, cli.metadata) {
                Ok(p) => p,
                Err(e) => {
                    eprintln!("error merging playlists: {e}");
                    return;
                }
            };
            if cli.purge {
                playlist.purge_duplicate(cli.purge_strata);
            }
            save_playlist(cli.dir, &playlist, cli.stdout)
        }
        PlaylistCmd::Japi { query } => playlist_from_query(gtr_query!(japi / query), cli),
        PlaylistCmd::Search { pages } => zworpshop_search(*pages, cli),
    }
}

fn zworpshop_search(pages: usize, cli: PlaylistCli) {
    let mut title = None;
    let mut author = None;
    let t = demand::Input::new("Search Title")
        .prompt(">")
        .run()
        .unwrap();
    if !t.is_empty() {
        title = Some(t);
    }
    let a = demand::Input::new("Search Author")
        .prompt(">")
        .run()
        .unwrap();
    if !a.is_empty() {
        author = Some(a);
    }
    let filter = match (author, title) {
        (None, None) => unreachable!("cannot have an empty search"),
        (None, Some(t)) => format!("contains(name,'{t}')"),
        (Some(a), None) => format!("contains(fileAuthor,'{a}')"),
        (Some(a), Some(t)) => format!("and(contains(name,'{t}'),equals(fileAuthor,'{a}'))"),
    };
    let mut levels = Vec::new();
    let mut next = format!("https://jsonapi.zworpshop.com/levels?filter={filter}");
    demand::Spinner::new("waiting for zworpshop")
        .run(|s| {
            while let Ok(res) = ureq::get(&next).call() {
                let res = serde_json::from_reader::<_, ZworpResponse>(res.into_reader()).unwrap();
                levels.extend(res.data.into_iter().map(|l| l.level));
                let _ = s.title(format!("found: {} levels", levels.len()));
                if levels.len() >= pages * 10 {
                    break;
                }
                let Some(n) = res.links.next else { break };
                next = n;
            }
        })
        .unwrap();
    if levels.is_empty() {
        return;
    }
    let l_opt = levels
        .into_iter()
        .map(|l| {
            demand::DemandOption::with_label(format!("{} by {}", l.name, l.file_author), l.into())
        })
        .collect();
    let levels = demand::MultiSelect::new("Select Levels")
        .options(l_opt)
        .filterable(true)
        .run()
        .unwrap();
    if levels.is_empty() {
        return;
    }
    let meta = cli.metadata.unwrap_or_else(interactive_playlist_metadata);
    let playlist = Playlist::new(meta, levels);
    save_playlist(cli.dir, &playlist, cli.stdout);
}

fn merge(
    one: &str,
    two: &str,
    dir: &Option<PathBuf>,
    meta: Option<PlaylistMeta>,
) -> Result<Playlist> {
    // let mut one = load_playlist(&one, dir)?;
    let dir = dir.clone().or_else(get_playlist_dir).unwrap();
    let mut one = load_playlist(one, &dir)?;
    // let two = load_playlist(&two, dir)?;
    let two = load_playlist(two, &dir)?;
    one.merge(two);
    if let Some(m) = meta {
        one.meta = m;
    } else if demand::Confirm::new("Metadata")
        .description(&format!(
            "edit playlist Metadata (uses {}'s data if no) [y/n]: ",
            one.get_name()
        ))
        .affirmative("Edit")
        .negative("No")
        .run()
        .expect("error getting user input")
    {
        one.meta = interactive_playlist_metadata();
    }
    Ok(one)
}

fn load_playlist(p: &str, dir: &Path) -> Result<Playlist> {
    // check current dir
    let mut path = PathBuf::from(p);
    path.set_extension("zeeplist");
    if path.is_file() {
        let file = fs::read_to_string(path)?;
        return Ok(serde_json::from_str(&file)?);
    }
    // check supplied dir
    let mut path = PathBuf::from(dir);
    path.push(p);
    path.set_extension("zeeplist");
    if path.is_file() {
        let file = fs::read_to_string(path)?;
        return Ok(serde_json::from_str(&file)?);
    }
    bail!("could not find: {p}");
}

fn interactive_playlist_metadata() -> PlaylistMeta {
    let name = demand::Input::new("Playlist Name")
        .prompt("> ")
        .placeholder("Playlist")
        .suggestions(&["Favorite", "Hot", "Popular", "Pb's", "Wr's", "Random"])
        .validation(|s| {
            if s.trim().is_empty() {
                Err("name cannot be empty")
            } else {
                Ok(())
            }
        })
        .run()
        .expect("error getting user input")
        .trim()
        .to_string();

    let round_length = loop {
        if let Ok(n) = demand::Input::new("Round Length")
            .description("in seconds")
            .prompt("> ")
            .validation(|s| {
                (s != "." && s.chars().all(|c| c.is_numeric() || c == '.'))
                    .then_some(())
                    .ok_or("must be a number")
            })
            .run()
            .expect("error getting user input")
            .parse()
        {
            break n;
        }
        println!("not a valid number");
    };

    let shuffle = demand::Confirm::new("Shuffle Playlist")
        .run()
        .expect("error getting user input");

    PlaylistMeta {
        name,
        round_length,
        shuffle,
    }
}

pub fn get_id(ty: Option<IdType>, id: Option<u64>) {
    let (ty, id) = match (ty, id) {
        (None, None) => {
            let Some(i) = get_latest_steam_player() else {
                eprintln!("could not get the latest player");
                return;
            };
            (IdType::Steam, i)
        }
        (None, Some(_)) => unreachable!("cli will fail ty requires a value"),
        (Some(_), None) => unreachable!("cli will fail if ty is some but id is none"),
        (Some(t), Some(i)) => (t, i),
    };
    let query = match ty {
        IdType::Steam => gtr_query!(usr / steam, id),
        IdType::Discord => gtr_query!(usr / discord, id),
        IdType::Gtr => gtr_query!(usr / gtr, id),
    };
    let r: gtr::Id = match fetch_query(query) {
        Ok(r) => {
            let Ok(r) = serde_json::from_reader(r.into_reader()) else {
                eprintln!("error parsing response");
                return;
            };
            r
            // serde_json::from_reader(r.into_reader()).unwrap()
        }
        _ => {
            eprintln!("error fetching id");
            return;
        }
    };
    println!("=== User ===");
    println!("gtr id: {}", r.gtr_id);
    println!("steam id: {}", r.steam_id);
    println!("steam name: {}", r.steam_name);
    println!(
        "discord id: {}",
        r.discord_id.unwrap_or("not linked".to_owned())
    );
}
