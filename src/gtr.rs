use serde::{de::DeserializeOwned, Deserialize};
use thiserror::Error;

use crate::Level;

pub mod favorite;
pub mod hash;
pub mod id;
pub mod pb;
pub mod random;
pub mod workshop;
pub mod wr;

pub const API: &str = "https://api.zeepkist-gtr.com/";
pub const GAPI: &str = "https://graphql.zeepkist-gtr.com";
pub const JAPI: &str = "https://jsonapi.zeepkist-gtr.com/";

pub trait Levels {
    fn levels(&self) -> &[Level];
    fn levels_mut(&mut self) -> &mut [Level];
    fn take_levels(self) -> Vec<Level>;
    fn total_levels(&self) -> Option<usize>;
    fn skipped_levels(&self) -> Option<usize>;
}

pub trait GqlQuery: Sized + std::fmt::Debug {
    type Response: DeserializeOwned;
    fn query(&self) -> String;
    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>>;

    fn body(&self) -> serde_json::Value {
        if let Some(vars) = self.variables() {
            serde_json::json!({
                "query": self.query(),
                "variables": vars,
            })
        } else {
            serde_json::json!({
                "query": self.query(),
            })
        }
    }

    // fn pagnation(&self, response: &Self::Response) -> Option<Self> {
    //     None
    // }

    fn call(&self) -> Result<Self::Response, Error> {
        let body = self.body();
        let r = match ureq::post(GAPI)
            .set("Content-Type", "application/json")
            .send_string(&serde_json::to_string(&body)?)
        {
            Ok(r) => r,
            Err(e) => match e {
                ureq::Error::Status(400, r) => {
                    let body = serde_json::to_string(&body).unwrap();
                    let err: GqlErrorResponse = serde_json::from_reader(r.into_reader())?;
                    return Err(Error::GqlError(body, err.errors));
                }
                ureq::Error::Status(_, _) | ureq::Error::Transport(_) => return Err(e.into()),
            },
        };
        Ok(serde_json::from_reader(r.into_reader())?)
    }
}

#[derive(Deserialize, Debug)]
struct GqlErrorResponse {
    errors: Vec<GqlError>,
}

#[derive(Deserialize, Debug)]
pub struct GqlError {
    pub message: String,
    pub locations: Vec<GqlLocation>,
}

#[derive(Deserialize, Debug)]
pub struct GqlLocation {
    pub line: usize,
    pub column: usize,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("there is an error with the gql query")]
    GqlError(String, Vec<GqlError>),
    #[error("error deserialising json {0}")]
    Serde(#[from] serde_json::Error),
    #[error("error sending request {0}")]
    Ureq(Box<ureq::Error>),
}

impl From<ureq::Error> for Error {
    fn from(value: ureq::Error) -> Self {
        Self::Ureq(Box::new(value))
    }
}

#[macro_export]
macro_rules! gtr_query {
    //mam<S-X>ms(izeeper::<S-Q>uery::<S-G>tr<C-space>
    (favorites, $id:expr, $limit:expr, $offset:expr, $steamid:expr) => {
        zeeper::Query::Gtr(format!(
            "{}favorites?{}={}&Limit={}&Offset={}",
            zeeper::gtr::API,
            if $steamid { "UserSteamId" } else { "UserId" },
            $id,
            $limit,
            $offset,
        ))
    };
    (levels/hot) => {
        zeeper::Query::Gtr(format!("{}levels/hot", zeeper::gtr::API,))
    };
    (levels/popular) => {
        zeeper::Query::Gtr(format!("{}levels/popular", zeeper::gtr::API,))
    };
    (wr / usr, $id:expr, $limit:expr, $offset:expr) => {
        zeeper::Query::Gtr(format!(
            "{}wrs/user/{}?Limit={}&Offset={}",
            zeeper::gtr::API,
            $id,
            $limit,
            $offset
        ))
    };
    (pb / usr, $id:expr, $limit:expr, $offset:expr) => {
        zeeper::Query::Gtr(format!(
            "{}pbs/user/{}?Limit={}&Offset={}",
            zeeper::gtr::API,
            $id,
            $limit,
            $offset
        ))
    };
    (records/recent, $limit:expr, $offset:expr) => {
        zeeper::Query::Gtr(format!(
            "{}records/recent?Limit={}&Offset={}",
            zeeper::gtr::API,
            $limit,
            $offset
        ))
    };
    (records/wr, $id:expr, $limit:expr, $offset:expr, $sort:expr, $steamid:expr) => {
        zeeper::Query::Gtr(format!(
            "{}records?{}={}&WorldRecordOnly=true&Limit={}&Offset={}{}",
            zeeper::gtr::API,
            if $steamid { "UserSteamId" } else { "UserId" },
            $id,
            $limit,
            $offset,
            if let Some(sort) = $sort {
                format!("&Sort={sort}")
            } else {
                format!("")
            }
        ))
    };
    (records/usr, $id:expr, $limit:expr, $offset:expr, $sort:expr, $steamid:expr) => {
        zeeper::Query::Gtr(format!(
            "{}records?{}={}&BestOnly=true&Limit={}&Offset={}{}",
            zeeper::gtr::API,
            if $steamid { "UserSteamId" } else { "UserId" },
            $id,
            $limit,
            $offset,
            if let Some(sort) = $sort {
                format!("&Sort={sort}")
            } else {
                format!("")
            }
        ))
    };
    (usr/steam, $id:expr) => {
        zeeper::Query::Gtr(format!("{}users/steam/{}", zeeper::gtr::API, $id))
    };
    (usr/discord, $id:expr) => {
        zeeper::Query::Gtr(format!("{}users/discord/{}", zeeper::gtr::API, $id))
    };
    (usr/gtr, $id:expr) => {
        zeeper::Query::Gtr(format!("{}users/{}", zeeper::gtr::API, $id))
    };
    (japi/$query:expr) => {
        zeeper::Query::Gtr(format!("{}{}", zeeper::gtr::JAPI, $query))
    };
}
