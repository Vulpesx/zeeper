use serde::Deserialize;

pub const API: &str = "https://api.zworpshop.com/";

#[derive(Debug)]
pub struct Level {
    pub file_uid: String,
    pub workshop_id: u32,
    pub file_author: String,
    pub file_hash: String,
    pub name: String,
    pub deleted: bool,
}

impl<'de> Deserialize<'de> for Level {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize, Debug)]
        #[serde(untagged)]
        enum StringOrU32 {
            String(String),
            U32(u32),
        }
        #[derive(Deserialize, Debug)]
        #[serde(rename_all = "camelCase")]
        pub struct Inner {
            pub file_uid: String,
            pub workshop_id: StringOrU32,
            pub file_author: String,
            pub file_hash: String,
            pub name: String,
            pub deleted: bool,
        }
        let i = Inner::deserialize(deserializer)?;
        Ok(Self {
            file_uid: i.file_uid,
            workshop_id: match i.workshop_id {
                StringOrU32::String(s) => s.trim().parse().map_err(serde::de::Error::custom)?,
                StringOrU32::U32(u) => u,
            },
            file_author: i.file_author,
            file_hash: i.file_hash,
            name: i.name,
            deleted: i.deleted,
        })
    }
}

#[derive(Deserialize)]
pub struct JLevel {
    #[serde(rename = "attributes")]
    pub level: Level,
}

#[derive(Deserialize)]
pub struct Links {
    #[serde(rename = "self")]
    pub this: String,
    pub first: Option<String>,
    pub last: Option<String>,
    pub next: Option<String>,
}

#[derive(Deserialize)]
pub struct ZworpResponse {
    pub links: Links,
    pub data: Vec<JLevel>,
}

#[macro_export]
macro_rules! zworp_query {
    //mam<S-X>ms(izeeper::<S-Q>uery::<S-Z>worpshop<C-space>
    (levels/random, $amount:expr) => {
        zeeper::Query::Zworpshop(format!(
            "{}levels/random?Amount={}",
            zeeper::zworpshop::API,
            $amount
        ))
    };
    (levels/hashes, $replaced:expr, $deleted:expr, $hashes:expr) => {
        zeeper::Query::Zworpshop(format!(
            "{}levels/hashes/{}?IncludeReplaced={}&IncludeDeleted={}",
            zeeper::zworpshop::API,
            $hashes,
            $replaced,
            $deleted,
        ))
    };
    (levels/workshop, $ids:expr, $replaced:expr, $deleted:expr) => {
        zeeper::Query::Zworpshop(format!(
            "{}levels/workshops/{}?IncludeReplaced={}&IncludeDeleted={}",
            zeeper::zworpshop::API,
            $ids,
            $replaced,
            $deleted
        ))
    };
    (levels, $limit:expr, $offset:expr) => {
        zeeper::Query::Zworpshop(format!(
            "{}levels?limit={}&offset={}",
            zeeper::zworpshop::API,
            $limit,
            $offset
        ))
    };
}
