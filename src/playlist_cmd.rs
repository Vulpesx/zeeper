use anyhow::{bail, Result};
use std::{
    fs,
    path::{Path, PathBuf},
};

use zeeper::{
    gtr::{self, GqlError, GqlQuery, Levels},
    playlist::{PlaylistMeta, PurgeStrata},
    Id, Playlist,
};

use crate::cli::{IdCmd, PlaylistArgs, PlaylistCli};

fn get_latest_steam_player() -> Option<u64> {
    let sd = steamlocate::SteamDir::locate().ok()?;
    let (app, _) = sd.find_app(1440670).ok()??;
    app.last_user
}

#[cfg(unix)]
fn get_playlist_dir() -> Option<PathBuf> {
    let sd = steamlocate::SteamDir::locate().ok()?;
    Some(sd.path()
                .join("steamapps/compatdata/1440670/pfx/drive_c/users/steamuser/AppData/Roaming/Zeepkist/Playlists"))
}

#[cfg(target_os = "windows")]
fn get_playlist_dir() -> Option<PathBuf> {
    dirs::data_dir().map(|d| d.join("Zeepkist/Playlists"))
}

#[cfg(not(any(unix, target_os = "windows")))]
fn get_playlist_dir() -> Option<PathBuf> {
    eprintln!("cannot find playlist dir on this platform");
    eprintln!("provide a path with cli arg or env variable");
    None
}

fn save_playlist(path: Option<PathBuf>, playlist: &Playlist, stdout: bool) {
    let playlist_out = match serde_json::to_string_pretty(playlist) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("error saving playlist {e}");
            return;
        }
    };
    if stdout {
        println!("{playlist_out}");
        return;
    }
    let Some(mut path) = path.or_else(get_playlist_dir) else {
        eprintln!("could not get the playlist dir");
        return;
    };
    if path.is_dir() {
        path.push(playlist.get_name());
    }
    path.set_extension("zeeplist");

    if let Err(e) = fs::write(&path, playlist_out) {
        eprintln!("error saving playlist {e}");
        return;
    };
    println!("Saved: {path:?}");
}

pub fn run_cmd(cli: PlaylistCli) {
    match cli.cmd {
        // crate::cli::PlaylistCmd::Favorite {
        //     ref id,
        //     limit,
        //     offset,
        // } => {
        // TODO: favorites exist in the api, but its empty and the mod
        // doesnt have it yet so lets wait also idk if the query is correct anyway
        // let id = match id {
        //     Some(id) => id.clone().into(),
        //     None => {
        //         let Some(id) = get_latest_steam_player() else {
        //             eprintln!("couldnt get latest steam user");
        //             return;
        //         };
        //         Id::Steam(id)
        //     }
        // };
        // playlist_from_query(gtr::favorite::FavQuery { id, limit, offset }, cli);
        // }

        // TODO: yoink rtm filtering lol
        // TODO: advanced search filters and shit,
        // mostly waiting on a good cli prompt
        crate::cli::PlaylistCmd::Random { amount: limit } => {
            playlist_from_query(gtr::random::RandQuery { limit }, cli.args);
        }
        crate::cli::PlaylistCmd::Wr { id, limit } => {
            let id = match id {
                Some(i) => i.into(),
                None => {
                    let Some(id) = get_latest_steam_player() else {
                        eprintln!("couldnt get latest steam user");
                        return;
                    };
                    Id::Steam(id)
                }
            };
            playlist_from_query(gtr::wr::WrQuery { id, limit }, cli.args);
        }
        crate::cli::PlaylistCmd::Pb { id, limit } => {
            let id = match id {
                Some(id) => id.into(),
                None => {
                    let Some(id) = get_latest_steam_player() else {
                        eprintln!("couldnt get latest steam user");
                        return;
                    };
                    Id::Steam(id)
                }
            };
            playlist_from_query(
                gtr::pb::PbQuery {
                    id,
                    limit,
                    // end_cursor: None,
                },
                cli.args,
            );
        }
        // TODO: advanced search filters and shit,
        // mostly waiting on a good cli prompt
        // crate::cli::PlaylistCmd::Search { pages } => todo!(),
        crate::cli::PlaylistCmd::Hash { hashes } => {
            if hashes.is_empty() {
                println!("no hashes provided. exiting");
                return;
            }
            playlist_from_query(gtr::hash::HashQuery { hashes }, cli.args)
        }
        crate::cli::PlaylistCmd::Workshop { wids } => {
            if wids.is_empty() {
                println!("no id's provided. exiting");
                return;
            }
            playlist_from_query(gtr::workshop::WorkshopQuery { wids }, cli.args)
        }
        crate::cli::PlaylistCmd::Merge { one, two } => merge(&one, &two, cli.args),
    }
}

fn merge(one: &str, two: &str, args: PlaylistArgs) {
    let Some(dir) = args.dir.or_else(get_playlist_dir) else {
        eprintln!("could not get playlist dir");
        return;
    };
    let mut one = match load_playlist(one, &dir) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };
    let two = match load_playlist(two, &dir) {
        Ok(o) => o,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };
    one.merge(two);
    if let Some(meta) = args.metadata {
        one.meta = meta;
    } else if demand::Confirm::new("Metadata")
        .description(&format!(
            "edit playlist Metadata (uses {}'s data if no) [y/n]: ",
            one.get_name()
        ))
        .affirmative("Edit")
        .negative("No")
        .run()
        .expect("error getting user input")
    {
        one.meta = interactive_playlist_metadata();
    }
    if args.purge {
        println!(
            "purged {} duplicates",
            one.purge_duplicate(args.purge_strata)
        );
    }
    save_playlist(Some(dir), &one, args.stdout)
}

fn load_playlist(name: &str, dir: &Path) -> Result<Playlist> {
    let mut path = PathBuf::from(name);
    path.set_extension("zeeplist");
    if path.is_file() {
        let file = fs::read_to_string(&path)?;
        return Ok(serde_json::from_str(&file)?);
    }
    let mut path = dir.join(name);
    path.set_extension("zeeplist");
    if path.is_file() {
        let file = fs::read_to_string(&path)?;
        return Ok(serde_json::from_str(&file)?);
    }
    bail!("could not find playlist")
}

fn playlist_from_query<R: Levels + Send, Q: GqlQuery<Response = R> + Send>(
    query: Q,
    cli: PlaylistArgs,
) {
    let r = demand::Spinner::new("getting data...")
        .run(move |_| query.call())
        .unwrap();
    let (skipped, total, levels) = match r {
        Ok(r) => (r.skipped_levels(), r.total_levels(), r.take_levels()),
        Err(e) => {
            eprintln!("error fetching levels: {e}");
            if let gtr::Error::GqlError(b, e) = e {
                print_gql_erros(b, e);
            }
            return;
        }
    };
    if let Some(s) = skipped {
        println!("skipped: {s}");
    }
    if let Some(t) = total {
        println!("{}/{t} levels", levels.len());
    }
    let meta = cli.metadata.unwrap_or_else(interactive_playlist_metadata);
    let mut playlist = Playlist::new(meta, levels);
    if cli.purge {
        println!(
            "purged {} tracks",
            playlist.purge_duplicate(cli.purge_strata)
        );
    } else if demand::Confirm::new("Purge")
        .description("remove duplicates")
        .run()
        .expect("error getting user input")
    {
        // let strata = demand::Select::new("Strata")
        //     .description("purge strategy")
        //     .options(vec![
        //         demand::DemandOption::with_label("Hash (default)", PurgeStrata::Hash),
        //         demand::DemandOption::with_label("Uid (old)", PurgeStrata::Uid),
        //     ])
        //     .filterable(true)
        //     .run()
        //     .unwrap_or(PurgeStrata::Hash);
        println!(
            "purged {} tracks",
            playlist.purge_duplicate(PurgeStrata::Uid)
        );
    }

    save_playlist(cli.dir, &playlist, cli.stdout);
}

pub fn get_id(id: Option<IdCmd>) {
    let id = match id {
        Some(i) => i.into(),
        None => {
            let Some(id) = get_latest_steam_player() else {
                eprintln!("could not get the latest steam user");
                return;
            };
            Id::Steam(id)
        }
    };
    match (gtr::id::IdQuery { id }.call()) {
        Ok(r) => {
            println!("=== User ===");
            println!("gtr id: {}", r.gtr_id);
            println!("steam id: {}", r.steam_id);
            println!("steam name: {}", r.steam_name);
            if let Some(id) = r.discord_id {
                println!("discord id: {id}",);
            } else {
                println!("discord id: not linked",);
            }
        }
        Err(e) => {
            eprintln!("{e}");
            if let gtr::Error::GqlError(b, es) = e {
                print_gql_erros(b, es);
            }
        }
    }
}

fn print_gql_erros(_body: String, errors: Vec<GqlError>) {
    eprintln!("--- GQL ERRORS ---");
    for error in errors {
        eprintln!("message: {}", error.message);
        eprint!("locations: [");
        for loc in error.locations {
            eprint!("{{line: {}, col: {}}}", loc.line, loc.column);
        }
    }
}

fn interactive_playlist_metadata() -> PlaylistMeta {
    let name = demand::Input::new("Playlist Name")
        .prompt("> ")
        .placeholder("Playlist")
        .suggestions(&["Favorite", "Hot", "Popular", "Pb's", "Wr's", "Random"])
        .validation(|s| {
            if s.trim().is_empty() {
                Err("name cannot be empty")
            } else {
                Ok(())
            }
        })
        .run()
        .expect("error getting user input")
        .trim()
        .to_string();

    let round_length = loop {
        if let Ok(n) = demand::Input::new("Round Length")
            .description("in seconds")
            .prompt("> ")
            .validation(|s| {
                (s != "." && s.chars().all(|c| c.is_numeric() || c == '.'))
                    .then_some(())
                    .ok_or("must be a number")
            })
            .run()
            .expect("error getting user input")
            .parse()
        {
            break n;
        }
        println!("not a valid number");
    };

    let shuffle = demand::Confirm::new("Shuffle Playlist")
        .run()
        .expect("error getting user input");

    PlaylistMeta {
        name,
        round_length,
        shuffle,
    }
}
