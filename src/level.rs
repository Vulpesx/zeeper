use serde::{Deserialize, Serialize};

use crate::zworpshop;

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct Level {
    #[serde(rename = "UID")]
    pub uid: String,
    #[serde(rename = "WorkshopID")]
    pub workshop_id: u32,
    pub name: String,
    pub author: String,
}

impl From<zworpshop::Level> for Level {
    fn from(lvl: zworpshop::Level) -> Self {
        Self {
            uid: lvl.file_uid,
            workshop_id: lvl.workshop_id,
            name: lvl.name,
            author: lvl.file_author,
        }
    }
}
