use serde::Deserialize;

use crate::Level;

use super::{GqlQuery, Levels};

#[derive(Debug)]
pub struct RandQuery {
    pub limit: u32,
}

impl GqlQuery for RandQuery {
    type Response = RandResponse;

    fn query(&self) -> String {
        "query Rand($first: Int) {allSampledLevelItems(first: $first) {nodes {fileUid fileAuthor name workshopId}}}".to_owned()
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        map.insert("first".into(), self.limit.into());
        Some(map)
    }
}

pub struct RandResponse {
    levels: Vec<Level>,
    skipped: usize,
}

impl Levels for RandResponse {
    fn levels(&self) -> &[Level] {
        &self.levels
    }

    fn levels_mut(&mut self) -> &mut [Level] {
        &mut self.levels
    }

    fn take_levels(self) -> Vec<Level> {
        self.levels
    }

    fn total_levels(&self) -> Option<usize> {
        None
    }

    fn skipped_levels(&self) -> Option<usize> {
        Some(self.skipped)
    }
}

impl<'de> Deserialize<'de> for RandResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allSampledLevelItems: AllSampledLevelItems,
        }
        #[derive(Deserialize)]
        struct AllSampledLevelItems {
            nodes: Vec<Node>,
        }
        #[derive(Deserialize)]
        struct Node {
            fileUid: String,
            fileAuthor: String,
            name: String,
            workshopId: String,
        }
        let r = Response::deserialize(deserializer)?;
        let mut skipped = 0;
        Ok(Self {
            levels: r
                .data
                .allSampledLevelItems
                .nodes
                .into_iter()
                .filter_map(|n| {
                    let Ok(workshop_id) = n.workshopId.parse() else {
                        skipped += 1;
                        return None;
                    };
                    Some(Level {
                        uid: n.fileUid,
                        workshop_id,
                        name: n.name,
                        author: n.fileAuthor,
                    })
                })
                .collect(),
            skipped,
        })
    }
}
