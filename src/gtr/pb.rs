use serde::Deserialize;

use crate::{Id, Level};

use super::{GqlQuery, Levels};

#[derive(Debug)]
pub struct PbQuery {
    pub id: Id,
    pub limit: usize,
}

impl GqlQuery for PbQuery {
    type Response = PbResponse;

    fn query(&self) -> String {
        let (name, id, condition) = match self.id {
            Id::Steam(_) => ("PbSteam", "$id: BigFloat", "{steamId: {equalTo:$id}}"),
            Id::Discord(_) => ("PbDiscord", "$id: BigFloat", "{discordId: {equalTo:$id}}"),
            Id::Gtr(_) => ("PbGtr", "$id: Int", "{id: {equalTo:$id}}"),
        };
        format!("query {name}($first: Int,{id}){{allPersonalBestGlobals(first: $first, filter: {{userByIdUser: {condition}}}){{totalCount nodes{{levelByIdLevel{{levelItemsByIdLevel{{nodes{{fileUid fileAuthor workshopId name}}}}}}}}}}}}")
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        match &self.id {
            Id::Steam(id) => map.insert("id".into(), id.to_string().into()),
            Id::Discord(id) => map.insert("id".into(), id.to_string().into()),
            Id::Gtr(id) => map.insert("id".into(), (*id).into()),
        };
        map.insert("first".into(), self.limit.into());
        Some(map)
    }
}

pub struct PbResponse {
    levels: Vec<Level>,
    skipped: usize,
    total: usize,
}

impl Levels for PbResponse {
    fn levels(&self) -> &[crate::Level] {
        &self.levels
    }

    fn levels_mut(&mut self) -> &mut [crate::Level] {
        &mut self.levels
    }

    fn take_levels(self) -> Vec<crate::Level> {
        self.levels
    }

    fn total_levels(&self) -> Option<usize> {
        Some(self.total)
    }

    fn skipped_levels(&self) -> Option<usize> {
        Some(self.skipped)
    }
}

impl<'de> Deserialize<'de> for PbResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allPersonalBestGlobals: AllPersonalBestGlobals,
        }
        #[derive(Deserialize)]
        struct AllPersonalBestGlobals {
            totalCount: usize,
            nodes: Vec<AllPersonalBestGlobalsNode>,
        }
        #[derive(Deserialize)]
        struct AllPersonalBestGlobalsNode {
            levelByIdLevel: LevelByIdLevel,
        }
        #[derive(Deserialize)]
        struct LevelByIdLevel {
            levelItemsByIdLevel: LevelItemsByIdLevel,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevel {
            nodes: Vec<LevelItemsByIdLevelNode>,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevelNode {
            workshopId: String,
            name: String,
            fileAuthor: String,
            fileUid: String,
        }
        let r = Response::deserialize(deserializer)?;
        let total = r.data.allPersonalBestGlobals.totalCount;
        let mut skipped = 0;
        let levels = r
            .data
            .allPersonalBestGlobals
            .nodes
            .into_iter()
            .filter_map(|mut n| {
                let level = n.levelByIdLevel.levelItemsByIdLevel.nodes.swap_remove(0);
                let Ok(workshop_id) = level.workshopId.parse() else {
                    skipped += 1;
                    return None;
                };
                Some(Level {
                    uid: level.fileUid,
                    workshop_id,
                    name: level.name,
                    author: level.fileAuthor,
                })
            })
            .collect();
        Ok(Self {
            levels,
            skipped,
            total,
        })
    }
}
