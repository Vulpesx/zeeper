use serde::Deserialize;

use crate::Level;

use super::{GqlQuery, Levels};

#[derive(Debug)]
pub struct FavQuery {
    pub id: crate::Id,
    pub limit: usize,
    pub offset: usize,
}

impl GqlQuery for FavQuery {
    type Response = FavResponse;

    fn query(&self) -> String {
        let (name, var, condition) = match &self.id {
            crate::Id::Steam(_) => ("IdSteam", "$id: BigFloat", "{steamId: $id}"),
            crate::Id::Discord(_) => ("IdDiscord", "$id: BigFloat", "{discordId: $id}"),
            crate::Id::Gtr(_) => ("IdGtr", "$id: Int", "{id: $id}"),
        };
        format!("query {name}({var}, $first: Int, $offset: Int) {{allUsers(condition: {condition}, offset: $offset, first: $first) {{nodes {{favoritesByIdUser {{nodes {{levelByIdLevel {{hashlevelItemsByIdLevel {{nodes {{fileAuthorfileUidnameworkshopId}}}}}}}}}}}}}}}}")
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        match &self.id {
            crate::Id::Steam(i) => map.insert("id".into(), i.to_string().into()),
            crate::Id::Discord(i) => map.insert("id".into(), i.to_string().into()),
            crate::Id::Gtr(i) => map.insert("id".into(), (*i).into()),
        };
        map.insert("offset".into(), self.offset.into());
        map.insert("first".into(), self.limit.into());
        Some(map)
    }
}

pub struct FavResponse {
    pub levels: Vec<Level>,
}

impl Levels for FavResponse {
    fn levels(&self) -> &[Level] {
        &self.levels
    }

    fn levels_mut(&mut self) -> &mut [Level] {
        &mut self.levels
    }

    fn take_levels(self) -> Vec<Level> {
        self.levels
    }

    fn total_levels(&self) -> Option<usize> {
        todo!()
    }

    fn skipped_levels(&self) -> Option<usize> {
        todo!()
    }
}

impl<'de> Deserialize<'de> for FavResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allUsers: AllUsers,
        }
        #[derive(Deserialize)]
        struct AllUsers {
            nodes: Vec<AllUsersNode>,
        }
        #[derive(Deserialize)]
        struct AllUsersNode {
            favoritesByIdUser: FavoritesByIdUser,
        }
        #[derive(Deserialize)]
        struct FavoritesByIdUser {
            nodes: Vec<FavoritesByIdUserNode>,
        }
        #[derive(Deserialize)]
        struct FavoritesByIdUserNode {
            levelByIdLevel: LevelByIdLevel,
        }
        #[derive(Deserialize)]
        struct LevelByIdLevel {
            levelItemsByIdLevel: LevelItemsByIdLevel,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevel {
            nodes: Vec<LevelItemsByIdLevelNode>,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevelNode {
            fileAuthor: String,
            fileUid: String,
            name: String,
            workshopId: String,
        }
        let mut r = Response::deserialize(deserializer)?;
        match r.data.allUsers.nodes.len() {
            0 => {
                return Err(serde::de::Error::custom("no users found"));
            }
            1 => (),
            n => return Err(serde::de::Error::custom(format!("{n} users found"))),
        }
        let n = r.data.allUsers.nodes.remove(0);
        let levels = n
            .favoritesByIdUser
            .nodes
            .into_iter()
            .filter_map(|n| {
                // let hash = n.levelByIdLevel.hash;
                let stuff = n.levelByIdLevel.levelItemsByIdLevel.nodes.first()?;
                Some(Level {
                    uid: stuff.fileUid.clone(),
                    workshop_id: stuff.workshopId.parse().ok()?,
                    name: stuff.name.clone(),
                    author: stuff.fileAuthor.clone(),
                    // hash,
                })
            })
            .collect();
        Ok(Self { levels })
    }
}
