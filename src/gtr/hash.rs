use serde::Deserialize;

use crate::Level;

use super::{GqlQuery, Levels};

#[derive(Debug)]
pub struct HashQuery {
    pub hashes: Vec<String>,
}

impl GqlQuery for HashQuery {
    type Response = HashResponse;

    fn query(&self) -> String {
        "query Hashes($hashes:[String!]){allLevels(filter:{hash:{in:$hashes}}){nodes{levelItemsByIdLevel{nodes{name workshopId fileUid fileAuthor}}}}}".to_string()
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        map.insert("hashes".into(), self.hashes.clone().into());
        Some(map)
    }
}

pub struct HashResponse {
    levels: Vec<Level>,
    skipped: usize,
}

impl Levels for HashResponse {
    fn levels(&self) -> &[Level] {
        &self.levels
    }

    fn levels_mut(&mut self) -> &mut [Level] {
        &mut self.levels
    }

    fn take_levels(self) -> Vec<Level> {
        self.levels
    }

    fn total_levels(&self) -> Option<usize> {
        None
    }

    fn skipped_levels(&self) -> Option<usize> {
        Some(self.skipped)
    }
}

impl<'de> Deserialize<'de> for HashResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allLevels: AllLevels,
        }
        #[derive(Deserialize)]
        struct AllLevels {
            nodes: Vec<AllLevelsNode>,
        }
        #[derive(Deserialize)]
        struct AllLevelsNode {
            levelItemsByIdLevel: LevelItemsByIdLevel,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevel {
            nodes: Vec<LevelItemsByIdLevelNode>,
        }
        #[derive(Deserialize)]
        struct LevelItemsByIdLevelNode {
            name: String,
            workshopId: String,
            fileUid: String,
            fileAuthor: String,
        }
        let r = Response::deserialize(deserializer)?;
        let mut skipped = 0;
        Ok(Self {
            levels: r
                .data
                .allLevels
                .nodes
                .into_iter()
                .filter_map(|mut n| {
                    let Some(item) = n.levelItemsByIdLevel.nodes.pop() else {
                        skipped += 1;
                        return None;
                    };
                    let Ok(workshop_id) = item.workshopId.parse() else {
                        skipped += 1;
                        return None;
                    };
                    Some(Level {
                        uid: item.fileUid,
                        workshop_id,
                        name: item.name,
                        author: item.fileAuthor,
                    })
                })
                .collect(),
            skipped,
        })
    }
}
