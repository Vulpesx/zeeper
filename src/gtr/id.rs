use serde::Deserialize;

use super::GqlQuery;

#[derive(Debug)]
pub struct IdQuery {
    pub id: crate::Id,
}

impl GqlQuery for IdQuery {
    type Response = IdResponse;

    fn query(&self) -> String {
        let (name, vars, condition) = match &self.id {
            crate::Id::Steam(_) => ("IdSteam", "$id: BigFloat", "{steamId: $id}"),
            crate::Id::Discord(_) => ("IdDiscord", "$id: BigFloat", "{discordId: $id}"),
            crate::Id::Gtr(_) => ("IdGtr", "$id: Int", "{id: $id}"),
        };
        format!("query {name}({vars}) {{ allUsers(condition: {condition}) {{ nodes {{ discordId id steamId steamName }}}}}}")
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        match &self.id {
            crate::Id::Steam(i) => map.insert("id".into(), i.to_string().into()),
            crate::Id::Discord(i) => map.insert("id".into(), i.to_string().into()),
            crate::Id::Gtr(i) => map.insert("id".into(), (*i).into()),
        };
        Some(map)
    }
}

#[derive(Debug)]
pub struct IdResponse {
    pub gtr_id: u32,
    pub steam_id: String,
    pub steam_name: String,
    pub discord_id: Option<String>,
}

impl<'de> Deserialize<'de> for IdResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allUsers: AllUsers,
        }
        #[derive(Deserialize)]
        struct AllUsers {
            nodes: Vec<Node>,
        }
        #[derive(Deserialize)]
        struct Node {
            discordId: Option<String>,
            id: u32,
            steamId: String,
            steamName: String,
        }

        let mut res = Response::deserialize(deserializer)?;
        match res.data.allUsers.nodes.len() {
            0 => {
                return Err(serde::de::Error::custom("no users found"));
            }
            1 => (),
            n => return Err(serde::de::Error::custom(format!("{n} users found"))),
        }
        let n = res.data.allUsers.nodes.remove(0);
        Ok(Self {
            gtr_id: n.id,
            steam_id: n.steamId,
            steam_name: n.steamName,
            discord_id: n.discordId.and_then(|s| (!s.starts_with('-')).then_some(s)),
        })
    }
}
