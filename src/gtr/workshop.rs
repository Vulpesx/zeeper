use serde::Deserialize;

use crate::Level;

use super::{GqlQuery, Levels};

#[derive(Debug)]
pub struct WorkshopQuery {
    pub wids: Vec<String>,
}

impl GqlQuery for WorkshopQuery {
    type Response = WorkshopResponse;

    fn query(&self) -> String {
        "query Workshop($wid:[BigFloat!]){allLevelItems(filter:{workshopId:{in:$wid}}){nodes{workshopId name fileAuthor fileUid}}}".to_owned()
    }

    fn variables(&self) -> Option<serde_json::Map<String, serde_json::Value>> {
        let mut map = serde_json::Map::new();
        map.insert("wid".into(), self.wids.clone().into());
        Some(map)
    }
}

pub struct WorkshopResponse {
    levels: Vec<Level>,
    skipped: usize,
}

impl Levels for WorkshopResponse {
    fn levels(&self) -> &[Level] {
        &self.levels
    }

    fn levels_mut(&mut self) -> &mut [Level] {
        &mut self.levels
    }

    fn take_levels(self) -> Vec<Level> {
        self.levels
    }

    fn total_levels(&self) -> Option<usize> {
        None
    }

    fn skipped_levels(&self) -> Option<usize> {
        Some(self.skipped)
    }
}

impl<'de> Deserialize<'de> for WorkshopResponse {
    #[allow(non_snake_case)]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct Response {
            data: Data,
        }
        #[derive(Deserialize)]
        struct Data {
            allLevelItems: AllLevelItems,
        }
        #[derive(Deserialize)]
        struct AllLevelItems {
            nodes: Vec<AllLevelItemsNode>,
        }
        #[derive(Deserialize)]
        struct AllLevelItemsNode {
            workshopId: String,
            name: String,
            fileAuthor: String,
            fileUid: String,
        }
        let r = Response::deserialize(deserializer)?;
        let mut skipped = 0;
        Ok(Self {
            levels: r
                .data
                .allLevelItems
                .nodes
                .into_iter()
                .filter_map(|n| {
                    let Ok(workshop_id) = n.workshopId.parse() else {
                        skipped += 1;
                        return None;
                    };
                    Some(Level {
                        uid: n.fileUid,
                        workshop_id,
                        name: n.name,
                        author: n.fileAuthor,
                    })
                })
                .collect(),
            skipped,
        })
    }
}
