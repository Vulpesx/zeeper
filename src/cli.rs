use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};
use zeeper::{
    playlist::{PlaylistMeta, PurgeStrata},
    Id,
};

use clap_complete_command::Shell;

#[derive(Parser)]
#[command(version, author, styles(get_styles()))]
pub struct Cli {
    #[command(subcommand)]
    pub cmd: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
    /// commands for playlists
    #[command(visible_alias = "p")]
    Playlist {
        #[command(flatten)]
        args: PlaylistCli,
    },
    /// gets your GTR id
    Id {
        #[command(subcommand)]
        id: Option<IdCmd>,
    },
    /// command for mod things
    #[command(visible_alias = "m")]
    Mod {
        #[command(subcommand)]
        cmd: ModCmd,
        /// modio api key. can also save it with login cmd
        ///
        /// get key from https://mod.io/me/access
        #[arg(long, short, env = "ZEEP_MOD_KEY")]
        key: Option<String>,
        /// zeepkist install location
        #[arg(long, short, env = "ZEEPKIST_DIR")]
        dir: Option<PathBuf>,
    },
    /// generate shell completions
    Completions { shell: Shell },
}

#[derive(Subcommand)]
pub enum ModCmd {
    /// save api key
    #[command(visible_alias = "l")]
    Login,
    /// list installed mods
    ///
    /// only needs api key if there is no zeeper.json available
    #[command(visible_alias = "ls")]
    List,
    /// check for updates
    ///
    /// needs api key
    #[command(visible_alias = "c")]
    Check,
    /// update mods
    ///
    /// needs api key
    #[command(visible_alias = "u")]
    Update,
    /// search and install mod
    ///
    /// needs api key
    #[command(visible_alias = "i")]
    Install,
    /// quickly re install multiple mods
    ///
    /// needs api key
    #[command(visible_alias = "ri")]
    ReInstall,
    /// find installed mods
    ///
    /// needs api key
    #[command(visible_alias = "f")]
    Find,
    /// remove a mod
    ///
    /// doesnt need api key
    #[command(visible_alias = "rm")]
    Remove,
    /// install a local mod file
    ///
    /// doesnt need api key
    #[command(visible_alias = "s")]
    Sideload { path: PathBuf },
    /// Install and setup bepinex
    InstallBepinex {
        /// just run post installation setup for linux
        #[arg(short, long)]
        linux_setup: bool,
        #[arg(short, long)]
        flatpak: bool,
    },

    #[command(visible_alias = "p")]
    Profile {
        #[command(subcommand)]
        cmd: ProfileCmd,
    },
}

#[derive(Subcommand)]
pub enum ProfileCmd {
    #[command(visible_alias = "n")]
    Create {
        name: Option<String>,
        #[arg(short, long)]
        dont_copy: bool,
    },
    #[command(visible_alias = "rm")]
    Delete { name: Option<String> },
    #[command(visible_alias = "s")]
    Change { name: Option<String> },
    #[command(visible_alias = "ls")]
    List,
}

#[derive(Subcommand, Clone)]
/// specify id for gtr to use
pub enum IdCmd {
    /// steam id
    Steam { id: u64 },
    /// discord id
    Discord { id: String },
    /// gtr id
    Gtr { id: u32 },
}

impl From<IdCmd> for Id {
    fn from(value: IdCmd) -> Self {
        match value {
            IdCmd::Steam { id } => Self::Steam(id),
            IdCmd::Discord { id } => Self::Discord(id),
            IdCmd::Gtr { id } => Self::Gtr(id),
        }
    }
}

#[derive(Args)]
pub struct PlaylistCli {
    /// the playlist command
    #[command(subcommand)]
    pub cmd: PlaylistCmd,

    #[command(flatten)]
    pub args: PlaylistArgs,
}

#[derive(Args)]
pub struct PlaylistArgs {
    /// output dir
    #[arg(short, long, env = "ZEEP_PLAYLIST_DIR")]
    pub dir: Option<PathBuf>,

    /// purge duplicates
    ///
    /// this could remove non duplicates
    #[arg(long)]
    pub purge: bool,

    /// purge stratagy
    ///
    /// - uid compares the uid of levels, tends to miss duplicates this is the old stratagy
    ///
    /// - hash compares the actual contents of the levels, much more accurate
    ///
    /// either could in theory purge non duplicates
    #[arg(long, default_value = "uid")]
    // #[arg(long, default_value = "hash")]
    pub purge_strata: PurgeStrata,

    /// print to stdout instead of saving to a file
    #[arg(long)]
    pub stdout: bool,

    #[arg(short, long)]
    pub metadata: Option<PlaylistMeta>,
}

#[derive(Subcommand)]
pub enum PlaylistCmd {
    // /// users Favorite tracks
    // #[command(visible_alias = "fav")]
    // Favorite {
    //     /// User id, needed to get your favorites
    //     // #[arg(env = "ZEEP_ID")]
    //     #[command(subcommand)]
    //     id: Option<IdCmd>,

    //     /// the max number of tracks, to get all favorites set to a higher or equal amount
    //     #[arg(short, long, default_value_t = 100)]
    //     limit: usize,
    //     /// skips the first 'n' tracks
    //     #[arg(short, long, default_value_t = 0)]
    //     offset: usize,
    // },
    /// Random tracks
    #[command(visible_alias = "rand")]
    Random {
        /// max amount of tracks
        #[arg(default_value_t = 10)]
        amount: u32,
    },
    // /// gets hot tracks in your area :3
    // Hot,
    // /// gets popular tracks
    // #[command(visible_alias = "pop")]
    // Popular,
    /// get tracks that user has wr on
    Wr {
        /// User id
        // #[arg(env = "ZEEP_GTR_ID")]
        #[command(subcommand)]
        id: Option<IdCmd>,
        /// the max amount of tracks
        #[arg(short, long, default_value_t = 100)]
        limit: usize,
        // /// skip n first tracks
        // #[arg(short, long, default_value_t = 0)]
        // offset: usize,
    },
    /// get tracks that user has pb on
    Pb {
        /// User id
        // #[arg(env = "ZEEP_GTR_ID")]
        #[command(subcommand)]
        id: Option<IdCmd>,
        /// the max amount of tracks
        #[arg(short, long, default_value_t = 100)]
        limit: usize,
        // /// skip n first tracks
        // #[arg(short, long, default_value_t = 0)]
        // offset: usize,
    },
    // /// make a playlist by searching zworpshop
    // #[command(visible_alias = "s")]
    // Search {
    //     #[arg(short, long, default_value_t = 4)]
    //     pages: usize,
    // },
    /// make playlist from zworpshop hashes
    Hash {
        /// separated list of hashes
        hashes: Vec<String>,
    },
    /// make playlist from workshop ids
    Workshop {
        /// separated list of ids
        wids: Vec<String>,
    },
    /// merge two playlists
    Merge {
        /// file
        one: String,
        /// file
        two: String,
    },
    // /// make a jsonapi request to gtr servers
    // ///
    // /// see https://jsonapi.zeepkist-gtr.com/swagger/index.html for
    // /// list of available end points and https://jsonapi.org/format/#fetching
    // /// for what you can do with those endpoints
    // /// dont include the whole url just provide the endpoint and query
    // /// eg `levelpoints?sort=-points&page[size]=100`
    // /// for 100 levels with the most points
    // Japi { query: String },
}

pub fn get_styles() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .usage(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
        )
        .header(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
        )
        .literal(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Blue))),
        )
        .invalid(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .error(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Red))),
        )
        .valid(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Green))),
        )
        .placeholder(
            anstyle::Style::new().fg_color(Some(anstyle::Color::Ansi(anstyle::AnsiColor::Magenta))),
        )
}
