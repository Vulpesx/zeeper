pub mod ghub;
pub mod gtr;
pub mod level;
pub mod modio;
pub mod playlist;
pub mod zworpshop;

pub use level::Level;
pub use playlist::Playlist;

use thiserror::Error;

#[derive(Debug, Clone)]
pub enum Id {
    Steam(u64),
    Discord(String),
    Gtr(u32),
}

#[derive(Debug)]
pub enum Query {
    Gtr(String),
    Zworpshop(String),
}

impl Query {
    pub fn value(self) -> String {
        match self {
            Query::Gtr(s) => s,
            Query::Zworpshop(s) => s,
        }
    }
}

#[derive(Error, Debug)]
pub enum CmdErr {
    #[error("this command requires an api key")]
    NoApiKey,
}

#[macro_export]
macro_rules! err {
    // this is first to catch err!("message {a}") and work how youd expect
    ($e:literal $(, $a:expr)*) => {
        // brackets make it work as an expression
        {
            eprintln!("[{} {}:{}]", file!(), line!(), column!());
            eprintln!($e $(,$a)*);
        }
    };
    ($($e:expr),*) => {
        {
            eprintln!("[{} {}:{}]", file!(), line!(), column!());
            $(
            eprintln!("{}", $e);
            )*
        }
    }
}
