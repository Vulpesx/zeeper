use std::{
    collections::HashMap,
    fs,
    io::Cursor,
    path::{Path, PathBuf},
    process::Command,
};

use copy_dir::copy_dir;
use dirs::data_local_dir;
use serde::{Deserialize, Serialize};
use zeeper::{
    err, ghub,
    modio::{self, Mod},
};

use crate::cli::{ModCmd, ProfileCmd};

const MANIFEST: &str = "zeeper.json";
// TODO: make sure to update this on any changes
const MANIFEST_VERSION: u32 = 1;

#[derive(Deserialize, Serialize)]
struct Manifest {
    version: u32,
    profile: Option<String>,
    mods: Vec<ModState>,
    blueprints: Vec<BlueprintState>,
    sideloads: Vec<SideloadState>,
}

impl Manifest {
    fn iter_info_or_path(&self) -> impl Iterator<Item = InfoOrPath> {
        self.iter_info()
            .map(InfoOrPath::Info)
            .chain(self.sideloads.iter().map(|s| InfoOrPath::Path(&s.path)))
    }

    fn iter_info(&self) -> impl Iterator<Item = ModInfo> {
        self.mods
            .iter()
            .map(|m| ModInfo {
                mod_id: &m.mod_id,
                file_id: &m.file_id,
                name: &m.name,
                version: &m.version,
                path: &m.path,
                kind: ModType::Mod,
            })
            .chain(self.blueprints.iter().map(|b| ModInfo {
                mod_id: &b.mod_id,
                file_id: &b.file_id,
                name: &b.name,
                version: &b.version,
                path: &b.path,
                kind: ModType::Blueprint,
            }))
    }

    // fn iter_info_mut(&mut self) -> impl Iterator<Item = ModInfoMut> {
    //     self.mods
    //         .iter_mut()
    //         .map(|m| ModInfoMut {
    //             mod_id: &mut m.mod_id,
    //             file_id: &mut m.file_id,
    //             name: &mut m.name,
    //             version: &mut m.version,
    //             path: &mut m.path,
    //             kind: ModType::Mod,
    //         })
    //         .chain(self.blueprints.iter_mut().map(|b| ModInfoMut {
    //             mod_id: &mut b.mod_id,
    //             file_id: &mut b.file_id,
    //             name: &mut b.name,
    //             version: &mut b.version,
    //             path: &mut b.path,
    //             kind: ModType::Blueprint,
    //         }))
    // }
}

#[derive(Deserialize, Serialize, Clone, Copy)]
enum ModType {
    Mod,
    Blueprint,
}

#[allow(dead_code)]
struct ModInfoMut<'a> {
    mod_id: &'a mut u32,
    file_id: &'a mut u32,
    name: &'a mut String,
    version: &'a mut String,
    path: &'a mut PathBuf,
    kind: ModType,
}

#[allow(dead_code)]
struct ModInfo<'a> {
    mod_id: &'a u32,
    file_id: &'a u32,
    name: &'a str,
    version: &'a str,
    path: &'a Path,
    kind: ModType,
}

enum InfoOrPath<'a> {
    Info(ModInfo<'a>),
    Path(&'a Path),
}

#[derive(Deserialize, Serialize)]
struct ModState {
    mod_id: u32,
    file_id: u32,
    name: String,
    version: String,
    path: PathBuf,
}

#[derive(Deserialize, Serialize)]
struct BlueprintState {
    mod_id: u32,
    file_id: u32,
    name: String,
    version: String,
    path: PathBuf,
}

#[derive(Debug, Deserialize, Serialize)]
struct SideloadState {
    path: PathBuf,
}

struct InstallMod {
    mod_id: u32,
    file_id: u32,
    file_size: usize,
    file_hash: String,
    name: String,
    version: String,
    kind: ModType,
    is_dependency: bool,
}

impl From<Mod> for InstallMod {
    fn from(value: Mod) -> Self {
        let kind = if value.tags.iter().any(|f| f == "Blueprint") {
            ModType::Blueprint
        } else {
            ModType::Mod
        };
        Self {
            mod_id: value.id,
            file_id: value.mod_file.id,
            file_size: value.mod_file.file_size,
            file_hash: value.mod_file.file_hash,
            name: value.name,
            version: value.mod_file.version,
            kind,
            is_dependency: false,
        }
    }
}

pub fn get_zeepkist_dir() -> Option<PathBuf> {
    let sd = steamlocate::SteamDir::locate().ok()?;
    let (app, lib) = sd.find_app(1440670).ok()??;

    Some(lib.resolve_app_dir(&app))
}

pub fn get_plugin_dir(dir: &Path) -> PathBuf {
    let pd = dir.join("BepInEx/plugins");
    match fs::create_dir(&pd) {
        Ok(_) => {}
        Err(e) if e.kind() == std::io::ErrorKind::AlreadyExists => {}
        Err(e) => {
            eprintln!("error creating plugins directory: {e}");
        }
    };
    pd
}

fn get_api_key() -> Option<String> {
    get_data_dir().and_then(|f| fs::read_to_string(f.join("api.key")).ok())
}

fn write_api_key(key: &str) {
    if let Some(p) = get_data_dir() {
        fs::create_dir_all(&p)
            .and_then(|_| fs::write(p.join("api.key"), key))
            .expect("couldnt save api key");
    }
}

fn write_manifest(dir: &Path, manifest: &Manifest) {
    serde_json::to_string_pretty(manifest)
        .map_err(anyhow::Error::from)
        .and_then(|s| Ok(fs::write(dir.join(MANIFEST), s)?))
        .expect("error updating manifest");
}

fn get_data_dir() -> Option<PathBuf> {
    data_local_dir().map(|f| f.join("zeeper"))
}

fn get_manifest(dir: &Path) -> Option<Manifest> {
    if let Ok(s) = fs::read(dir.join(MANIFEST)) {
        serde_json::from_slice::<Manifest>(&s).ok().filter(|m| {
            let v = m.version == MANIFEST_VERSION;
            if !v {
                println!("unsupported manifest version");
            }
            v
        })
    } else {
        None
    }
}

fn find_mods(dir: &Path, key: &str) -> Manifest {
    let Ok((mods, blueprints, sideloads)) =
        demand::Spinner::new("finding mods").run(|_| find_mods_inner(dir, key, FindState::None))
    else {
        eprintln!("error finding mods");
        std::process::exit(1);
    };
    let profile = match get_manifest(dir) {
        Some(p) => p.profile,
        None => {
            eprintln!("error reading manifest");
            None
        }
    };

    let man = Manifest {
        version: MANIFEST_VERSION,
        profile,
        mods,
        blueprints,
        sideloads,
    };
    write_manifest(dir, &man);
    return man;

    fn sideload(sideloads: &mut Vec<SideloadState>, name: &str, path: PathBuf) {
        if demand::Confirm::new(format!("Sideload {name}?"))
            .description("is this a mod or blueprint")
            .run()
            .unwrap_or(false)
        {
            sideloads.push(SideloadState { path });
        }
    }

    enum FindState {
        None,
        Mods,
        Blueprints,
        Sideloads,
    }

    fn find_mods_inner(
        dir: &Path,
        key: &str,
        state: FindState,
    ) -> (Vec<ModState>, Vec<BlueprintState>, Vec<SideloadState>) {
        let (mut mods, mut blueprints, mut sideloads) = (Vec::new(), Vec::new(), Vec::new());
        for d in fs::read_dir(dir).expect("couldnt read dir") {
            let Ok(d) = d else {
                // cause this is a closure not a loop we return
                continue;
            };
            let n = d.file_name();
            let n = n.to_string_lossy();
            match (&*n, &state) {
                ("Mods", FindState::None) => {
                    let (m, b, s) = find_mods_inner(&d.path(), key, FindState::Mods);
                    mods.extend(m);
                    blueprints.extend(b);
                    sideloads.extend(s);
                }
                ("Blueprints", FindState::None) => {
                    let (m, b, s) = find_mods_inner(&d.path(), key, FindState::Blueprints);
                    mods.extend(m);
                    blueprints.extend(b);
                    sideloads.extend(s);
                }
                ("Sideload", FindState::None) => {
                    sideloads.extend(find_mods_inner(&d.path(), key, FindState::Sideloads).2)
                }
                ("zeeper.json", FindState::None) => continue,
                (name, _) => {
                    let Some((m, f)) = name.split_once('_') else {
                        sideload(&mut sideloads, name, d.path());
                        continue;
                    };
                    let Ok(mod_id) = m.trim().parse() else {
                        sideload(&mut sideloads, name, d.path());
                        continue;
                    };
                    let Ok(file_id) = f.trim().parse() else {
                        sideload(&mut sideloads, name, d.path());
                        continue;
                    };
                    let r#mod = match modio::get_mod(mod_id, key) {
                        Ok(m) => m,
                        Err(modio::Error::NotFound) => {
                            sideload(&mut sideloads, name, d.path());
                            continue;
                        }
                        Err(e) => {
                            eprintln!("\nmod: {e}");
                            continue;
                        }
                    };
                    let mod_file = match modio::get_mod_file(mod_id, file_id, key) {
                        Ok(f) => f,
                        Err(e) => {
                            eprintln!("\nmod file: {e}");
                            continue;
                        }
                    };

                    let path = d.path();
                    let version = mod_file.version;
                    let name = r#mod.name;

                    if r#mod.tags.iter().any(|t| t == "Blueprint") {
                        blueprints.push(BlueprintState {
                            mod_id,
                            file_id,
                            name,
                            version,
                            path,
                        });
                    } else {
                        mods.push(ModState {
                            mod_id,
                            file_id,
                            name,
                            version,
                            path,
                        });
                    };
                }
            }
        }
        (mods, blueprints, sideloads)
    }
}

fn login() {
    if get_api_key().is_some()
        && !demand::Confirm::new("you already have a key")
            .affirmative("continue")
            .negative("cancel")
            .run()
            .unwrap_or(false)
    {
        return;
    }
    requires_api_key(None);
}

fn check(dir: &Path, key: &str) {
    let man = get_manifest(dir).unwrap_or_else(|| find_mods(dir, key));
    let Ok(updates) = demand::Spinner::new("chcecking for updates").run(|_| {
        man.iter_info()
            .filter_map(|m| {
                let mod_r = match modio::get_mod(*m.mod_id, key) {
                    Ok(m) => m,
                    Err(e) => {
                        eprintln!("{e}");
                        return None;
                    }
                };
                if m.file_id != &mod_r.mod_file.id {
                    Some((mod_r, m.name, m.version))
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
    }) else {
        eprintln!("error checking for updates");
        return;
    };

    if updates.is_empty() {
        println!("no updates found");
    } else {
        for (new, name, version) in updates {
            println!(
                "{}: {} -> {}: {}",
                name, version, new.name, new.mod_file.version
            );
        }
    }
}

fn update(dir: &Path, key: &str) {
    let mut man = get_manifest(dir).unwrap_or_else(|| find_mods(dir, key));
    let mods = demand::Spinner::new("Checking for updates")
        .run(|_| {
            man.iter_info()
                .filter_map(|m| {
                    let m_r = match modio::get_mod(*m.mod_id, key) {
                        Ok(m) => m,
                        Err(e) => {
                            eprintln!("{e}");
                            return None;
                        }
                    };
                    if m.file_id != &m_r.mod_file.id {
                        return Some((m, InstallMod::from(m_r)));
                    }
                    None
                })
                .collect::<Vec<_>>()
        })
        .unwrap();
    if mods.is_empty() {
        println!("no updates found");
        return;
    }
    let opts = mods
        .into_iter()
        .map(|(old, new)| {
            demand::DemandOption::with_label(
                format!(
                    "{}: {} -> {}: {}",
                    old.name, old.version, new.name, new.version
                ),
                (new.mod_id, new),
            )
        })
        .collect();

    let mut mods: HashMap<_, _> = demand::MultiSelect::new("Update Mods")
        .description("select mods to update")
        .options(opts)
        .filterable(true)
        .run()
        .unwrap_or_default()
        .into_iter()
        .collect();

    demand::Spinner::new("updating mods")
        .run(|s| {
            let start_num = mods.len();
            let _ = s.title("getting dependencies");
            for k in mods.keys().cloned().collect::<Vec<_>>() {
                collect_dependencies(k, key, &mut mods);
                let _ = s.title(format!("found {} dependencies", mods.len() - start_num));
            }

            for mi in mods.into_values() {
                let _ = s.title(format!("installing {}", mi.name));
                install_mod(mi, dir, &mut man, false);
            }
        })
        .unwrap();

    write_manifest(dir, &man);
}

fn remove(dir: &Path, man: &mut Manifest) {
    enum Id {
        ModId(u32, ModType),
        Path(PathBuf),
    }
    let opts = man
        .iter_info_or_path()
        .filter_map(|s| {
            let (label, id) = match s {
                InfoOrPath::Info(i) => (
                    format!("{}: {}", i.name, i.version),
                    Id::ModId(*i.mod_id, i.kind),
                ),

                InfoOrPath::Path(p) => {
                    let name = p.file_name()?.to_string_lossy();
                    (format!("{}", name), Id::Path(p.to_owned()))
                }
            };
            let o = demand::DemandOption::with_label(label, id);
            Some(o)
        })
        .collect();
    let Ok(mods) = demand::MultiSelect::new("remove mods")
        .description("choose mods to remove")
        .options(opts)
        .filterable(true)
        .run()
    else {
        eprintln!("error getting user input");
        return;
    };
    for id in mods {
        let path = match id {
            Id::ModId(mid, ModType::Mod) => {
                let mut path = None;
                man.mods.retain(|m| {
                    let r = m.mod_id == mid;
                    if r {
                        path = Some(m.path.clone());
                    }
                    !r
                });
                let Some(path) = path else {
                    eprintln!("somehow mod wasnt in manifest");
                    continue;
                };
                path
            }
            Id::ModId(mid, ModType::Blueprint) => {
                let mut path = None;
                man.blueprints.retain(|m| {
                    let r = m.mod_id == mid;
                    if r {
                        path = Some(m.path.clone());
                    }
                    !r
                });
                let Some(path) = path else {
                    eprintln!("somehow blueprint wasnt in manifest");
                    continue;
                };
                path
            }
            Id::Path(p) => {
                man.sideloads.retain(|s| s.path != p);
                p
            }
        };
        if path.exists() {
            if path.is_file() {
                fs::remove_file(path).unwrap();
            } else if path.is_dir() {
                fs::remove_dir_all(path).unwrap();
            } else {
                write_manifest(dir, man);
                unreachable!("{path:?} wasnt a file or dir");
            }
        }
    }
    write_manifest(dir, man);
}

fn sideload(path: PathBuf, dir: &Path, key: &str) {
    if !path.exists() {
        eprintln!("path is not a file or doesnt not exist");
        return;
    }

    let mut man = get_manifest(dir).unwrap_or_else(|| find_mods(dir, key));

    let name = path.file_name().expect("error getting file name");

    let new = dir.join("Sideload");
    fs::create_dir_all(&new).expect("error creating dir");
    let new = new.join(name);

    let mut found = false;
    if man.sideloads.iter().any(|i| i.path == new) {
        if !demand::Confirm::new(format!("Overwrite {}?", name.to_string_lossy()))
            .description("there is a sideload with the same name")
            .run()
            .expect("error getting user input")
        {
            return;
        }
        found = true;
    }

    if path.is_file() {
        fs::copy(path, &new).expect("error copying file");
    } else if path.is_dir() {
        fs::rename(path, &new).expect("error moving dir");
    } else {
        eprintln!("not a file or dir ?");
        return;
    };

    if !found {
        man.sideloads.push(SideloadState { path: new });
    }

    write_manifest(dir, &man);
}

fn collect_dependencies(mod_id: u32, key: &str, depends: &mut HashMap<u32, InstallMod>) {
    let mut pending = vec![mod_id];
    loop {
        let Some(mod_id) = pending.pop() else {
            break;
        };
        let dependencies = modio::dependencies(mod_id, key, true)
            .expect("error getting dependencies")
            .data;
        for m in dependencies {
            let depth = m.dependency_depth;
            let m = modio::get_mod(m.mod_id, key).expect("error parsing mod");
            let has_dependencies = m.has_dependencies;
            let m_id = m.id;
            depends.entry(m.id).or_insert(InstallMod {
                is_dependency: true,
                ..InstallMod::from(m)
            });
            if has_dependencies && depth == 5 {
                pending.push(m_id);
                // collect_dependencies(m_id, key, depends);
            }
        }
    }
}

pub fn install_cmd(dir: &Path, key: &str) {
    let mut man = get_manifest(dir).unwrap_or_else(|| find_mods(dir, key));

    let mods = loop {
        let query = match demand::Input::new("Search Modio")
            .prompt("search: ")
            .description("modio search can be a bit finicky")
            .run()
        {
            Ok(q) => q,
            Err(e) if e.kind() == std::io::ErrorKind::Interrupted => return,
            Err(e) => {
                err!("{e:?}");
                return;
            }
        };

        let tags = match modio::tags(key) {
            Ok(t) => t,
            Err(e) => {
                err!(e);
                return;
            }
        };
        let tags = match demand::MultiSelect::new("Tags")
            .description("choosing none will not filter tags")
            .options(tags.into_iter().map(demand::DemandOption::new).collect())
            .run()
        {
            Ok(t) => t,
            Err(e) => {
                err!(e);
                return;
            }
        };

        let mods = match modio::search((!query.is_empty()).then_some(&query), &tags, key) {
            Ok(m) => m,
            Err(e) => {
                err!(e);
                return;
            }
        }
        .data;

        if !mods.is_empty() {
            break mods;
        }
        match demand::Confirm::new("No Mods Found")
            .description("search again?")
            .run()
        {
            Ok(false) => return,
            Ok(_) => (),
            Err(e) => {
                err!(e);
                return;
            }
        }
    };
    let opt = mods
        .into_iter()
        .map(|m| {
            demand::DemandOption::with_label(
                format!("{}: {} [{}]", &m.name, &m.mod_file.version, &m.author),
                m,
            )
        })
        .collect();
    let Ok(mods) = demand::MultiSelect::new("install mods")
        .description("choose mods to install")
        .options(opt)
        .filterable(true)
        .run()
    else {
        eprintln!("error getting user input");
        return;
    };

    if !demand::Confirm::new(format!("Install {} mods", mods.len()))
        .affirmative("install")
        .negative("cancel")
        .run()
        .unwrap_or(false)
    {
        return;
    }

    let mut mod_map: HashMap<_, _> = mods
        .into_iter()
        .map(|m| {
            let m = InstallMod::from(m);
            (m.mod_id, m)
        })
        .collect();

    let spinner = demand::Spinner::new(format!("installing {} mods", mod_map.len()));

    spinner
        .run(|s| {
            let start_num = mod_map.len();
            let _ = s.title("getting dependencies");
            for k in mod_map.keys().cloned().collect::<Vec<_>>() {
                collect_dependencies(k, key, &mut mod_map);
                let _ = s.title(format!("found {} dependencies", mod_map.len() - start_num));
            }

            for mi in mod_map.into_values() {
                let _ = s.title(format!("installing {}", mi.name));
                install_mod(mi, dir, &mut man, true);
            }

            let _ = s.title("saving manifest");
            write_manifest(dir, &man);
        })
        .expect("how did this happen");
}

pub fn reinstall_cmd(dir: &Path, key: &str) {
    let mut man = get_manifest(dir).unwrap_or_else(|| find_mods(dir, key));

    let opts = man
        .iter_info()
        .map(|m| demand::DemandOption::with_label(m.name, m))
        .collect();
    let Ok(mods) = demand::MultiSelect::new("Reinstall mods")
        .description("choose mods to reinstall")
        .options(opts)
        .filterable(true)
        .run()
    else {
        eprintln!("error getting user input");
        return;
    };

    if !demand::Confirm::new(format!("Reinstall {} mods", mods.len()))
        .affirmative("reinstall")
        .negative("cancel")
        .run()
        .unwrap_or(false)
    {
        return;
    }

    let mod_map: HashMap<_, _> = mods
        .into_iter()
        .filter_map(|mi| {
            let m = match modio::get_mod(*mi.mod_id, key) {
                Ok(m) => m,
                Err(e) => {
                    eprintln!("error fetching mod info: {e}");
                    return None;
                }
            };
            Some((m.id, InstallMod::from(m)))
        })
        .collect();

    let spinner = demand::Spinner::new(format!("reinstalling {} mods", mod_map.len()));

    spinner
        .run(|s| {
            for mi in mod_map.into_values() {
                let _ = s.title(format!("installing {}", mi.name));
                install_mod(mi, dir, &mut man, true);
            }

            let _ = s.title("saving manifest");
            write_manifest(dir, &man);
        })
        .expect("how did this happen");
}

fn install_mod(mi: InstallMod, dir: &Path, man: &mut Manifest, prompt_replace: bool) {
    let (mut dir, info) = match mi.kind {
        ModType::Mod => (
            dir.join("Mods"),
            man.mods
                .iter_mut()
                .map(|m| ModInfoMut {
                    mod_id: &mut m.mod_id,
                    file_id: &mut m.file_id,
                    name: &mut m.name,
                    version: &mut m.version,
                    path: &mut m.path,
                    kind: mi.kind,
                })
                .find(|i| *i.mod_id == mi.mod_id),
        ),
        ModType::Blueprint => (
            dir.join("Blueprints"),
            man.mods
                .iter_mut()
                .map(|m| ModInfoMut {
                    mod_id: &mut m.mod_id,
                    file_id: &mut m.file_id,
                    name: &mut m.name,
                    version: &mut m.version,
                    path: &mut m.path,
                    kind: mi.kind,
                })
                .find(|i| *i.mod_id == mi.mod_id),
        ),
    };
    if let Some(ref info) = info {
        if prompt_replace && !mi.is_dependency {
            if !demand::Confirm::new(format!("Reinstall {}: {}", mi.name, mi.version))
                .description("this will overwrite previous install")
                .run()
                .unwrap_or(false)
            {
                return;
            }
        } else if *info.file_id == mi.file_id {
            return;
        }

        let dir_name = format!("{}_{}", mi.mod_id, mi.file_id);
        dir.push(dir_name);
    } else {
        let dir_name = format!("{}_{}", mi.mod_id, mi.file_id);
        dir.push(dir_name);
    }

    let data = match modio::download(mi.mod_id, mi.file_id, Some(mi.file_size)) {
        Ok(data) => data,
        Err(e) => {
            eprintln!("error downloading mod {e}");
            return;
        }
    };
    let hash = md5::compute(&data);
    if mi.file_hash != format!("{hash:x}") {
        eprintln!("{}: corrupted file", mi.name);
        return;
    }
    if let Some(p) = dir.parent() {
        if !p.exists() {
            fs::create_dir_all(p).expect("error creating parent directories");
        }
    }
    zip_extract::extract(Cursor::new(data), &dir, true).expect("huh");

    if let Some(info) = info {
        *info.file_id = mi.file_id;
        *info.version = mi.version;
        info.name.clone_from(&mi.name);
        fs::remove_dir_all(info.path.as_path()).expect("error removing old version");
        *info.path = dir;
    } else {
        let mod_id = mi.mod_id;
        let file_id = mi.file_id;
        let name = mi.name.clone();
        let version = mi.version;
        let path = dir;

        match mi.kind {
            ModType::Mod => {
                man.mods.push(ModState {
                    mod_id,
                    file_id,
                    name,
                    version,
                    path,
                });
            }
            ModType::Blueprint => {
                man.blueprints.push(BlueprintState {
                    mod_id,
                    file_id,
                    name,
                    version,
                    path,
                });
            }
        }
    }
}

fn requires_api_key(key: Option<String>) -> String {
    key.unwrap_or_else(|| loop {
        let key = demand::Input::new("Api Key")
            .description(
                "this command needs an api key, you can get yours at https://mod.io/me/access",
            )
            .prompt("key: ")
            .password(true)
            .run()
            .expect("error getting user input");
        match modio::get_mod(3451972, &key) {
            Ok(_) => {
                write_api_key(&key);
                break key;
            }
            Err(modio::Error::Unauthorized) => eprintln!("invalid key"),
            Err(e) => err!(e),
        }
    })
}

fn list(man: &Manifest) {
    if !man.mods.is_empty() {
        println!("=== Mods ===");
        for ModState { name, version, .. } in man.mods.iter() {
            println!("{name}: {version}");
        }
    }
    if !man.blueprints.is_empty() {
        println!("=== Blueprints ===");
        for BlueprintState { name, version, .. } in man.blueprints.iter() {
            println!("{name}: {version}");
        }
    }
    if !man.sideloads.is_empty() {
        println!("=== Sideloads ===");
        for s in man.sideloads.iter() {
            let name = s
                .path
                .file_name()
                .expect("could not get file name")
                .to_string_lossy();
            println!("{name}");
        }
    }
}

fn has_bepinex(dir: &Path) -> bool {
    dir.join("BepInEx").exists()
}

fn linux_mod_fix(flatpak: bool) {
    let wine_cmd = "wine reg add 'HKCU\\Software\\Wine\\DllOverrides' /v winhttp /t REG_SZ /d native,builtin /f";
    let mut cmd = if flatpak {
        Command::new("flatpak")
    } else {
        Command::new("protontricks")
    };
    if flatpak {
        cmd.args(["run", "protontricks", "-c", wine_cmd, "1440670"])
    } else {
        cmd.args(["-c", wine_cmd, "1440670"])
    };
    match cmd
            .output()
        {
            Ok(o) => if o.status.success() {
                println!("if mods dont work, report it in the zeepkist modding discord\nhttps://discord.gg/KzgWuZDNnH");
            } else {
                eprint!("error running protontricks");
                if let Some(code) = o.status.code() {
                    eprint!(" (code {code})");
                }
                eprintln!("\n{}", String::from_utf8_lossy(&o.stderr));
            },
            Err(e) if e.kind() == std::io::ErrorKind::NotFound => eprintln!("protontricks not installed, if installed via flatpak use `--flatpak`, else install it then run 'zeeper mod install-bepinex --linux-setup"),
            Err(e) => eprintln!("an error occured running protontricks:\n{e}"),
        }
}

fn install_bepinex(dir: &Path, flatpak: bool) {
    let r = match ghub::releases("BepInEx", "BepInEx") {
        Ok(r) => r,
        Err(e) => {
            eprintln!("{e}");
            return;
        }
    };

    let Some(r) = r.iter().find(|r| r.target_commitish == "v5-lts") else {
        println!("could not find latest v5 release");
        return;
    };
    let Some(a) = r.assets.iter().find(|a| a.name.contains("win_x64")) else {
        println!("could not find correct download");
        return;
    };
    let mut data = Vec::with_capacity(a.size);
    let r = match ureq::get(&a.browser_download_url).call() {
        Ok(r) => r,
        Err(e) => {
            eprintln!("error downloading bepinex: {e}");
            return;
        }
    };

    if let Err(e) = r.into_reader().read_to_end(&mut data) {
        eprint!("error reading data: {e}");
        return;
    }

    if let Err(e) = zip_extract::extract(Cursor::new(data), dir, true) {
        eprintln!("error extracting bepinex: {e}");
        return;
    }

    #[cfg(unix)]
    {
        linux_mod_fix(flatpak);
    }
}

fn create_profile(name: &str, copy: bool, pdir: &Path, zeep_dir: &Path) {
    if pdir.exists() {
        eprintln!("profile with {name} already exists");
        return;
    }
    if let Err(e) = fs::create_dir_all(pdir) {
        eprintln!("error creating profile: {e}");
        return;
    }
    let ppdir = pdir.join("plugins");
    let zpdir = get_plugin_dir(zeep_dir);
    if copy {
        if let Err(e) = copy_dir(zeep_dir.join("BepInEx/config"), pdir.join("config")) {
            eprintln!("error copying config: {e}");
            return;
        }
        if let Err(e) = copy_dir(zpdir, &ppdir) {
            eprintln!("error copying plugins: {e}");
            return;
        }
        let man = match get_manifest(&ppdir) {
            Some(mut m) => {
                m.profile = Some(name.to_owned());
                m
            }
            None => Manifest {
                version: MANIFEST_VERSION,
                profile: Some(name.to_owned()),
                mods: vec![],
                blueprints: vec![],
                sideloads: vec![],
            },
        };
        write_manifest(&ppdir, &man);
    } else {
        if let Err(e) = fs::create_dir(pdir.join("config")) {
            eprintln!("error creating config dir: {e}");
            return;
        }
        if let Err(e) = fs::create_dir(&ppdir) {
            eprintln!("error creating plugins dir: {e}");
            return;
        }
        write_manifest(
            &ppdir,
            &Manifest {
                version: MANIFEST_VERSION,
                profile: Some(name.to_owned()),
                mods: vec![],
                blueprints: vec![],
                sideloads: vec![],
            },
        );
    }
    println!("created {name}");
}

fn choose_profile(pdir: &Path, title: &str) -> Option<String> {
    if let Ok(d) = fs::read_dir(pdir) {
        let profiles = d
            .filter_map(|e| e.ok().map(|e| e.file_name().to_string_lossy().into_owned()))
            .map(demand::DemandOption::new)
            .collect::<Vec<_>>();
        demand::Select::new(title).options(profiles).run().ok()
    } else {
        None
    }
}

fn delete_profile(name: &str, pdir: &Path) {
    if !pdir.exists() {
        eprintln!("profile {name} doesnt exist");
        return;
    }
    if !demand::Confirm::new("Are you sure?")
        .description("this is permenant")
        .run()
        .unwrap()
    {
        return;
    }
    if let Err(e) = fs::remove_dir_all(pdir) {
        eprintln!("error deleting {name}: {e}");
        return;
    }
    println!("deleted {name}")
}

fn change_profile(name: &str, pdir: &Path, zeep_dir: &Path) {
    if !pdir.exists() {
        eprintln!("profile {name} doesnt exist");
        return;
    }
    let confirm = match get_manifest(&get_plugin_dir(zeep_dir)) {
        Some(Manifest {
            profile: Some(p), ..
        }) => pdir.parent().map_or(true, |d| !d.join(p).exists()),
        _ => true,
    };
    if confirm
        && !demand::Confirm::new("Are you sure?")
            .description("you dont have a profile active, you might loose data")
            .run()
            .unwrap()
    {
        return;
    }
    let zconf = zeep_dir.join("BepInEx/config");
    let zplug = get_plugin_dir(zeep_dir);
    if let Err(e) = fs::remove_dir_all(&zconf) {
        eprintln!("error removing existing config: {e}");
        return;
    }
    if let Err(e) = fs::remove_dir_all(&zplug) {
        eprintln!("error removing existing plugins: {e}");
        return;
    }
    #[cfg(unix)]
    {
        if let Err(e) = std::os::unix::fs::symlink(pdir.join("config"), zconf) {
            eprintln!("error linking config: {e}");
            return;
        }
        if let Err(e) = std::os::unix::fs::symlink(pdir.join("plugins"), zplug) {
            eprintln!("error linking plugins: {e}");
        }
    }
    #[cfg(windows)]
    {
        if let Err(e) = std::os::windows::fs::symlink_file(pdir.join("config"), zconf) {
            eprintln!("error linking config: {e}");
            return;
        }
        if let Err(e) = std::os::windows::fs::symlink_file(pdir.join("plugins"), zplug) {
            eprintln!("error linking plugins: {e}");
        }
    }
    println!("changed to {name}");
}

const PROFILES_DIR: &str = "Profiles";
//TODO: handle errors
//TODO: prompts
fn run_pf_cmd(cmd: ProfileCmd, zeep_dir: &Path) {
    let Some(mut ddir) = get_data_dir() else {
        eprintln!("couldnt get data directory!");
        return;
    };
    ddir.push(PROFILES_DIR);
    match cmd {
        ProfileCmd::Create { name, dont_copy } => {
            let name = match name {
                Some(n) => n,
                None => demand::Input::new("Name:").run().unwrap(),
            };
            let copy = if !dont_copy {
                demand::Confirm::new("Copy current profile?")
                    .description("copy settings and mods")
                    .run()
                    .unwrap()
            } else {
                false
            };
            ddir.push(&name);
            create_profile(&name, copy, &ddir, zeep_dir);
        }
        ProfileCmd::Delete { name } => {
            //TODO: ask for confirmation
            //TODO: report error if profile doesnt exist
            let name = match name {
                Some(n) => n,
                None => match choose_profile(&ddir, "Profile to Delete") {
                    Some(p) => p,
                    None => {
                        eprintln!("error getting profiles");
                        return;
                    }
                },
            };
            ddir.push(&name);
            delete_profile(&name, &ddir);
        }
        ProfileCmd::Change { name } => {
            //TODO: ask for confirmation when:
            // there is no manifest
            // there is a manifest that doesnt have a profile
            // there is a manifest that has a profile that doesnt exist
            let name = match name {
                Some(n) => n,
                None => match choose_profile(&ddir, "Choose Profile") {
                    Some(p) => p,
                    None => {
                        eprintln!("error getting profiles");
                        return;
                    }
                },
            };
            ddir.push(&name);
            change_profile(&name, &ddir, zeep_dir);
        }
        ProfileCmd::List => {
            let Ok(d) = fs::read_dir(ddir) else {
                eprintln!("error reading profiles");
                return;
            };
            // let profiles = d
            //     .filter_map(|e| e.ok().map(|e| e.file_name().to_string_lossy().into_owned()))
            //     .collect::<Vec<_>>();
            println!("---Profiles---");
            for e in d {
                let Ok(e) = e else {
                    continue;
                };
                println!("{}", e.file_name().to_string_lossy());
            }
            if let Some(Manifest {
                profile: Some(p), ..
            }) = get_manifest(&get_plugin_dir(zeep_dir))
            {
                println!("\n---Curent---\n{p}");
            }
        }
    }
}

pub fn run_cmd(cli: ModCmd, key: Option<String>, dir: Option<PathBuf>) {
    let key = key.or_else(get_api_key);
    let Some(dir) = dir.or_else(get_zeepkist_dir) else {
        eprintln!("error finding zeepkist");
        return;
    };

    let mut just_installed_bepinex = false;
    if !has_bepinex(&dir)
        && !matches!(cli, ModCmd::InstallBepinex { .. })
        && demand::Confirm::new("Install BepInEx?")
            .description("cant find bepinex installation")
            .run()
            .expect("error getting user input")
    {
        let flatpak = demand::Confirm::new("is protontricks installed under flatpak")
            .run()
            .expect("error getting user input");
        install_bepinex(&dir, flatpak);
        just_installed_bepinex = true;
    }

    match cli {
        ModCmd::Login => login(),
        ModCmd::List => {
            let dir = get_plugin_dir(&dir);
            let man = get_manifest(&dir).unwrap_or_else(|| {
                let key = requires_api_key(key);
                find_mods(&dir, &key)
            });
            list(&man);
        }
        ModCmd::Check => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            check(&dir, &key);
        }
        ModCmd::Update => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            update(&dir, &key);
        }
        ModCmd::Install => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            install_cmd(&dir, &key);
        }
        ModCmd::ReInstall => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            reinstall_cmd(&dir, &key);
        }
        ModCmd::Find => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            find_mods(&dir, &key);
        }
        ModCmd::Remove => {
            let dir = get_plugin_dir(&dir);
            let mut man = get_manifest(&dir).unwrap_or_else(|| {
                let key = requires_api_key(key);
                find_mods(&dir, &key)
            });
            remove(&dir, &mut man);
        }
        ModCmd::Sideload { path } => {
            let dir = get_plugin_dir(&dir);
            let key = requires_api_key(key);
            sideload(path, &dir, &key);
        }
        ModCmd::InstallBepinex {
            linux_setup,
            flatpak,
        } => {
            if linux_setup {
                linux_mod_fix(flatpak);
                return;
            }
            if !just_installed_bepinex {
                if has_bepinex(&dir)
                    && !demand::Confirm::new("Re install BepInEx?")
                        .description("BepInEx is already installed")
                        .run()
                        .unwrap()
                {
                    return;
                }

                install_bepinex(&dir, flatpak)
            }
        }
        ModCmd::Profile { cmd } => {
            run_pf_cmd(cmd, &dir);
        }
    }
}
