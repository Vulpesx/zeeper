use serde::Deserialize;
use thiserror::Error;

const API: &str = "https://api.github.com";

#[derive(Deserialize, Debug)]
pub struct Release {
    pub target_commitish: String,
    pub assets: Vec<Asset>,
}

#[derive(Deserialize, Debug)]
pub struct Asset {
    pub name: String,
    pub size: usize,
    pub browser_download_url: String,
}

pub fn releases(owner: &str, repo: &str) -> Result<Vec<Release>, Error> {
    let r = ureq::get(&format!("{API}/repos/{owner}/{repo}/releases")).call()?;
    let r = serde_json::from_reader(r.into_reader())?;
    Ok(r)
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("error sending request: {0}")]
    Ureq(Box<ureq::Error>),
    #[error("error parsing response: {0}")]
    Serde(#[from] serde_json::Error),
}

impl From<ureq::Error> for Error {
    fn from(value: ureq::Error) -> Self {
        Self::Ureq(Box::new(value))
    }
}
