use std::str::FromStr;

use anyhow::bail;
use clap::ValueEnum;
use serde::{Deserialize, Serialize};

use crate::Level;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PlaylistMeta {
    pub name: String,
    #[serde(rename = "roundLength")]
    pub round_length: f32,
    #[serde(rename = "shufflePlaylist")]
    pub shuffle: bool,
}

impl FromStr for PlaylistMeta {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let Some((name, rest)) = s.split_once(',') else {
            bail!("MetaData needs 'name' 'round' 'shuffle' separated by ','");
        };
        let Some((round_length, shuffle)) = rest.split_once(',') else {
            bail!("MetaData needs 'name' 'round' 'shuffle' separated by ','");
        };
        let name = name.to_string();
        let round_length = round_length.parse()?;
        let shuffle = shuffle.to_lowercase() == "y";
        Ok(Self {
            name,
            round_length,
            shuffle,
        })
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Playlist {
    #[serde(flatten)]
    pub meta: PlaylistMeta,
    #[serde(rename = "amountOfLevels")]
    level_count: usize,
    levels: Vec<Level>,
}

// TODO: remove this, only one varient used
#[derive(ValueEnum, Clone, Copy, Debug)]
pub enum PurgeStrata {
    Uid,
    // Hash,
}

impl PurgeStrata {
    fn purge(&mut self, level: &Level, list: &[String]) -> bool {
        let mut i = list.iter();
        match self {
            PurgeStrata::Uid => i.any(|i| level.uid == *i),
            // PurgeStrata::Hash => i.any(|(_, h)| level.hash == *h),
        }
    }
}

impl Playlist {
    pub fn new(meta: PlaylistMeta, levels: Vec<Level>) -> Playlist {
        Self {
            meta,
            level_count: levels.len(),
            levels,
        }
    }

    pub fn purge_duplicate(&mut self, mut strategy: PurgeStrata) -> usize {
        let mut l = vec![];
        let mut i = 0;
        self.levels.retain(|p| {
            let s = strategy.purge(p, &l);
            if s {
                i += 1;
            } else {
                // l.push((p.uid.clone(), p.hash.clone()));
                l.push(p.uid.clone())
            }
            !s
        });
        self.level_count = self.levels.len();
        i
    }

    pub fn get_name(&self) -> &str {
        &self.meta.name
    }

    pub fn merge(&mut self, mut two: Playlist) {
        self.levels.append(&mut two.levels);
        self.level_count = self.levels.len();
    }
}
