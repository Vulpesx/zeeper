use clap::Parser;

mod cli;
mod mod_cmd;
mod playlist_cmd;

use std::io;

use clap::CommandFactory;
use cli::{Cli, Commands};

fn main() {
    let cli = Cli::parse();
    if let Err(e) = ctrlc::set_handler(|| {
        let _ = console::Term::stderr().show_cursor();
    }) {
        eprintln!("error setting ctrlc handler {e}");
        std::process::exit(1);
    };
    match cli.cmd {
        Commands::Playlist { args } => playlist_cmd::run_cmd(args),
        Commands::Id { id } => playlist_cmd::get_id(id),
        Commands::Mod { cmd, key, dir } => mod_cmd::run_cmd(cmd, key, dir),

        Commands::Completions { shell } => {
            let mut bcli = Cli::command();
            shell.generate(&mut bcli, &mut io::stdout());
        }
    }
}
