use anyhow::Result;
use serde::Deserialize;
use serde_repr::Deserialize_repr;
use thiserror::Error;

pub const API: &str = "https://api.mod.io/v1";
pub const GAME_ID: u16 = 3213;

#[derive(Deserialize, Debug)]
pub struct GetMods {
    pub data: Vec<Mod>,
    pub result_count: u32,
    pub result_offset: u32,
    pub result_limit: u32,
    pub result_total: u32,
}

#[derive(Debug)]
pub struct Mod {
    pub id: u32,
    pub status: ModStatus,
    pub visible: bool,
    pub author: String,
    pub name: String,
    pub url: String,
    pub mod_file: ModFile,
    pub has_dependencies: bool,
    pub tags: Vec<String>,
}

impl<'de> Deserialize<'de> for Mod {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct User {
            username: String,
        }

        #[derive(Deserialize)]
        struct Tag {
            name: String,
        }

        #[derive(Deserialize)]
        struct Pre {
            id: u32,
            status: ModStatus,
            visible: u8,
            submitted_by: User,
            name: String,
            profile_url: String,
            modfile: ModFile,
            dependencies: bool,
            tags: Vec<Tag>,
        }
        let p = Pre::deserialize(deserializer)?;
        Ok(Self {
            id: p.id,
            status: p.status,
            visible: p.visible == 1,
            author: p.submitted_by.username,
            name: p.name,
            url: p.profile_url,
            mod_file: p.modfile,
            has_dependencies: p.dependencies,
            tags: p.tags.into_iter().map(|t| t.name).collect(),
        })
    }
}

#[derive(Deserialize_repr, Debug)]
#[repr(u8)]
pub enum ModStatus {
    NotAccepted,
    Accepted,
    Deleted,
}

#[derive(Debug)]
pub struct ModFile {
    pub id: u32,
    pub mod_id: u32,
    pub file_hash: String,
    pub file_name: String,
    pub version: String,
    pub download_url: String,
    pub file_size: usize,
}

impl<'de> Deserialize<'de> for ModFile {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct FileHash {
            md5: String,
        }

        #[derive(Deserialize)]
        struct Download {
            binary_url: String,
        }
        #[derive(Deserialize)]
        struct Pre {
            id: u32,
            mod_id: u32,
            filehash: FileHash,
            filename: String,
            version: Option<String>,
            download: Download,
            filesize: usize,
        }
        let p = Pre::deserialize(deserializer)?;
        Ok(Self {
            id: p.id,
            mod_id: p.mod_id,
            file_hash: p.filehash.md5,
            file_name: p.filename,
            version: p.version.unwrap_or_else(|| String::from("No Version")),
            download_url: p.download.binary_url,
            file_size: p.filesize,
        })
    }
}

#[derive(Deserialize, Debug)]
pub struct ModDependency {
    pub mod_id: u32,
    pub name: String,
    pub dependency_depth: u32,
}

#[derive(Deserialize)]
pub struct GetDependencies {
    pub data: Vec<ModDependency>,
    pub result_count: u32,
    pub result_offset: u32,
    pub result_limit: u32,
    pub result_total: u32,
}

pub fn search(query: Option<&str>, tags: &[String], key: &str) -> Result<GetMods, Error> {
    let query = if let Some(q) = query {
        format!("&_q={q}")
    } else {
        String::new()
    };
    let tags = if !tags.is_empty() {
        format!("&tags-in={}", tags.join(","))
    } else {
        String::new()
    };
    let q = format!("{API}/games/{GAME_ID}/mods?api_key={key}{query}{tags}");
    let r = ureq::get(&q).call()?;
    Ok(serde_json::from_reader(r.into_reader())?)
}

#[derive(Debug, Deserialize)]
pub struct TagOption {
    name: String,
    tags: Vec<String>,
}

#[derive(Debug, Deserialize)]
pub struct GameObject {
    tag_options: Vec<TagOption>,
}

pub fn tags(key: &str) -> Result<Vec<String>, Error> {
    let q = format!("{API}/games/{GAME_ID}?api_key={key}");
    let game = ureq::get(&q).call()?;
    let game: GameObject = serde_json::from_reader(game.into_reader())?;

    Ok(game
        .tag_options
        .into_iter()
        .flat_map(|t| {
            if t.tags.is_empty() {
                vec![t.name].into_iter()
            } else {
                t.tags.into_iter()
            }
        })
        .collect())
}

pub fn dependencies(mod_id: u32, key: &str, recursive: bool) -> Result<GetDependencies, Error> {
    let q = format!(
        "{API}/games/{GAME_ID}/mods/{mod_id}/dependencies?api_key={key}&recursive={recursive}"
    );
    let r = ureq::get(&q).call()?;
    Ok(serde_json::from_reader(r.into_reader())?)
}

pub fn get_mod(mod_id: u32, key: &str) -> Result<Mod, Error> {
    let q = format!("{API}/games/{GAME_ID}/mods/{mod_id}?api_key={key}");
    let r = ureq::get(&q).call()?;
    Ok(serde_json::from_reader(r.into_reader())?)
}

pub fn get_mod_file(mod_id: u32, file_id: u32, key: &str) -> Result<ModFile, Error> {
    let q = format!("{API}/games/{GAME_ID}/mods/{mod_id}/files/{file_id}?api_key={key}");
    let r = ureq::get(&q).call()?;
    Ok(serde_json::from_reader(r.into_reader())?)
}

pub fn download(mod_id: u32, file_id: u32, file_size: Option<usize>) -> Result<Vec<u8>, Error> {
    let q = format!("{API}/games/{GAME_ID}/mods/{mod_id}/files/{file_id}/download");
    let r = ureq::get(&q).call()?;
    let mut data = if let Some(size) = file_size {
        Vec::with_capacity(size)
    } else {
        Vec::new()
    };
    r.into_reader().read_to_end(&mut data)?;
    Ok(data)
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("api key is invalid or expired")]
    Unauthorized,
    #[error("content not found")]
    NotFound,
    #[error("parse error: {0}")]
    Parse(#[from] serde_json::Error),
    #[error("ureq error: {0}")]
    UreqOther(Box<ureq::Error>),
    #[error("io error: {0}")]
    IOError(#[from] std::io::Error),
}

impl From<ureq::Error> for Error {
    fn from(value: ureq::Error) -> Self {
        match value {
            ureq::Error::Status(401, _) => Self::Unauthorized,
            ureq::Error::Status(404, _) => Self::NotFound,
            _ => Self::UreqOther(Box::new(value)),
        }
    }
}

#[macro_export]
macro_rules! mod_query {
    //mam<S-X>ms(izeeper::<S-Q>uery::<S-Z>worpshop<C-space>
    (mod / download, $mod_id: expr, $file_id:expr ) => {
        format!(
            "{}/games/{}/mods/{}/files/{}/download",
            zeeper::modio::API,
            zeeper::modio::GAME_ID,
            $mod_id,
            $file_id,
        )
    };
}
