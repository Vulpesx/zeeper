!#/bin/fish
cargo build -r
cargo build -r --target x86_64-pc-windows-gnu
cp ./target/release/zeeper ./zeeper-linux
cp ./target/x86_64-pc-windows-gnu/release/zeeper.exe ./zeeper-windows.exe
strip zeeper-linux
strip zeeper-windows.exe
